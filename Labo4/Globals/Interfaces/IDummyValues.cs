﻿namespace Globals.Interfaces
{
    public interface IDummyValues
    {
        void DummyProperties();
        void DummyCharacteristics();
        void DummyBuyersRenters();
        void DummyAgreements();
    }
}
