﻿using System;
using System.ComponentModel.DataAnnotations;

namespace DataLayer.Enumerations
{
    [Flags]
    public enum BuildingTypes
    {
        [Display(Name = "Appartement")]
        Appartement = 1,
        [Display(Name = "Gesloten Bebouwing")]
        GeslotenBebouwing = 2,
        [Display(Name = "Open Bebouwing")]
        OpenBebouwing = 3,
        [Display(Name = "Half-open Bebouwing")]
        HalfOpenBebouwing = 4
    }
}
