﻿using System;

namespace DataLayer.Enumerations
{
    [Flags]
    public enum GroundTypes
    {
        Weiland = 1,
        Bouwgrond = 2,
        Landbouwgrond = 3
    }
}
