﻿using DataLayer.Enumerations;
using MongoDB.Bson.Serialization.Attributes;
using System.ComponentModel.DataAnnotations;

namespace DataLayer.Model_Classes
{
    [BsonDiscriminator("panden")]
    public partial class Buildings : Properties
    {
        [BsonElement("type"), EnumDataType(typeof(BuildingTypes))]
        public BuildingTypes? Types { get; set; }
        [BsonElement("kamers")]
        public int? Rooms { get; set; }
        [BsonElement("huisnr"), BsonRequired]
        public int HouseNr { get; set; }
        [BsonElement("busnr")]
        public int? BoxNr { get; set; }
        [BsonElement("EPC")]
        public double? EPC { get; set; }

        public virtual Characteristics Characteristics { get; set; }
    }
}
