﻿using DataLayer.Patterns;
using MongoDB.Bson;
using MongoDB.Bson.Serialization.Attributes;
using System.Collections.Generic;

namespace DataLayer.Model_Classes
{
    [BsonDiscriminator("eigenschappen")]
    public class Characteristics : EntityBase
    {
        [BsonRequired]
        public override int Id { get; set; }
        [BsonElement("type"), BsonRequired]
        public string Type { get; set; }

        public virtual ICollection<Buildings> Buildings { get; set; }
    }
}
