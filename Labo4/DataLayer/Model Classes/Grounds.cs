﻿using DataLayer.Enumerations;
using MongoDB.Bson;
using MongoDB.Bson.Serialization.Attributes;
using System.ComponentModel.DataAnnotations;

namespace DataLayer.Model_Classes
{
    [BsonDiscriminator("gronden")]
    public class Grounds : Properties
    {
        [BsonElement("opp")]
        public double? Surface { get; set; }
        [EnumDataType(typeof(GroundTypes)), BsonElement("type"), BsonRequired]
        public GroundTypes Types { get; set; }
    }
}
