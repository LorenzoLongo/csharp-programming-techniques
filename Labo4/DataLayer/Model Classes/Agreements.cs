﻿using DataLayer.Patterns;
using MongoDB.Bson;
using MongoDB.Bson.Serialization.Attributes;
using System;

namespace DataLayer.Model_Classes
{
    [BsonDiscriminator("overeenkomsten")]
    [BsonKnownTypes(typeof(BuyAgreements), typeof(RentAgreements))]
    public abstract class Agreements : EntityBase
    {
        [BsonRequired]
        public override int Id { get; set; }
        [BsonElement("datum"), BsonRequired]
        public DateTime Date { get; set; }

        [BsonElement("kopers_huurders_id"), BsonRequired]
        public virtual BuyersRenters BuyersRenters { get; set; }
        [BsonElement("eigendom_id"), BsonRequired]
        public virtual Properties Properties { get; set; }
    }
}
