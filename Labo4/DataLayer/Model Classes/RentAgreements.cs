﻿using MongoDB.Bson.Serialization.Attributes;

namespace DataLayer.Model_Classes
{
    [BsonDiscriminator("verhuurovereenkomsten")]
    public partial class RentAgreements : Agreements
    {
        [BsonElement("vaste kosten")]
        public double? FixedCosts { get; set; }
        [BsonElement("waarborg"), BsonRequired]
        public double Guarantee { get; set; }
        [BsonElement("maandbedrag"), BsonRequired]
        public double MonthlyCost { get; set; }
    }
}
