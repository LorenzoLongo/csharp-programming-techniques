﻿using MongoDB.Bson.Serialization.Attributes;

namespace DataLayer.Model_Classes
{
    [BsonDiscriminator("verkoopsovereenkomsten")]
    public partial class BuyAgreements : Agreements
    {
        [BsonElement("verkoopprijs"), BsonRequired]
        public double SellingPrice { get; set; }
    }
}
