﻿using DataLayer.Patterns;
using MongoDB.Bson;
using MongoDB.Bson.Serialization.Attributes;
using System.Collections.Generic;

namespace DataLayer.Model_Classes
{
    [BsonDiscriminator("kopers_huurders")]
    public class BuyersRenters : EntityBase
    {
        [BsonRequired]
        public override int Id { get; set; }
        [BsonElement("naam"), BsonRequired]
        public string Name { get; set; }
        [BsonElement("voornaam"), BsonRequired]
        public string Surname { get; set; }

        public virtual List<Agreements> Agreements { get; set; }
    }
}
