﻿using DataLayer.Patterns;
using MongoDB.Bson;
using MongoDB.Bson.Serialization.Attributes;
using System.Collections.Generic;

namespace DataLayer.Model_Classes
{
    [BsonDiscriminator("eigendommen")]
    [BsonKnownTypes(typeof(Grounds), typeof(Buildings))]
    public abstract class Properties : EntityBase
    {
        [BsonRequired]
        public override int Id { get; set; }
        [BsonElement("gemeente"), BsonRequired]
        public string Town { get; set; }
        [BsonElement("straat"), BsonRequired]
        public string Street { get; set; }

        public virtual List<Agreements> Agreements { get; set; }
    }
}
