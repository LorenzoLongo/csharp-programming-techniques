﻿using DataLayer.Model_Classes;
using MongoDB.Driver;

namespace DataLayer.Patterns
{
    public class MongoDBContext
    {
        private MongoClient client;

        private IMongoDatabase Database { get; set; }
        public virtual IMongoCollection<Agreements> Agreements { get; set; }
        public virtual IMongoCollection<BuyersRenters> BuyersRenters { get; set; }
        public virtual IMongoCollection<Properties> Properties { get; set; }
        public virtual IMongoCollection<Characteristics> Characteristics { get; set; }
        public virtual IMongoCollection<Buildings> Buildings { get; set; }

        public MongoDBContext(string connectionString)
        {
            client = new MongoClient(connectionString);

            Database = client.GetDatabase("admin-labo4db");

            Agreements = Database.GetCollection<Agreements>("overeenkomsten");
            BuyersRenters = Database.GetCollection<BuyersRenters>("kopers_huurders");
            Properties = Database.GetCollection<Properties>("eigendommen");
            Characteristics = Database.GetCollection<Characteristics>("eigenschappen");
            Buildings = Database.GetCollection<Buildings>("panden");
        }
    }
}
