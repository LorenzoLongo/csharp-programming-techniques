﻿namespace DataLayer.Patterns
{
    // Class to provide IDs to all created tables
    public abstract class EntityBase
    {
        public abstract int Id { get; set; }
    }
}
