﻿using DataLayer.Enumerations;
using DataLayer.Model_Classes;
using DataLayer.Patterns;
using Globals.Interfaces;
using MongoDB.Driver;
using System;

namespace LogicLayer
{
    // Class which contains all of the methods to add values inside of the code first created database
    public class DummyValues : IDummyValues
    {
        private MongoDBContext dbContext;

        public DummyValues(MongoDBContext context)
        {
            this.dbContext = context;
        }

        // Method to add values to the Properties (eigendommen) table
        public void DummyProperties()
        {
            var newProperty = new Grounds() { Id = 1, Town = "Ninove", Street = "Ninovelaan", Types = GroundTypes.Bouwgrond };
            dbContext.Properties.InsertOneAsync(newProperty);

            var newProperty2 = new Buildings() { Id = 2, Town = "Aalter", Street = "Fictiestraat", Rooms = 4, HouseNr = 215 };
            dbContext.Properties.InsertOneAsync(newProperty2);

            var newProperty3 = new Buildings() { Id = 3, Town = "Aalst", Street = "Dummystraat", Types = BuildingTypes.Appartement, Rooms = 2, HouseNr = 33, BoxNr = 3 };
            dbContext.Properties.InsertOneAsync(newProperty3);

            var newProperty4 = new Grounds() { Id = 4, Town = "Sint-Denijs", Street = "Kortrijkseteenweg", Surface = 3000.79, Types = GroundTypes.Bouwgrond };
            dbContext.Properties.InsertOneAsync(newProperty4);

            var newProperty5 = new Grounds() { Id = 5, Town = "Wondelgem", Street = "Wondelgemstraat", Surface = 257.33, Types = GroundTypes.Weiland };
            dbContext.Properties.InsertOneAsync(newProperty5);

            var newProperty6 = new Buildings() { Id = 6, Town = "Ledeberg", Street = "Lederberglaan", Types = BuildingTypes.HalfOpenBebouwing, HouseNr = 79, EPC = 57.23 };
            dbContext.Properties.InsertOneAsync(newProperty6);
        }

        // Method to add values to the Characteristics (eigenschappen) table
        public void DummyCharacteristics()
        {
            var newChar = new Characteristics() { Id = 1, Type = "Tuin" };
            dbContext.Characteristics.InsertOneAsync(newChar);

            var newChar2 = new Characteristics() { Id = 2, Type = "Kelder" };
            dbContext.Characteristics.InsertOneAsync(newChar2);

            var newChar3 = new Characteristics() { Id = 3, Type = "Zolder" };
            dbContext.Characteristics.InsertOneAsync(newChar3);

            var newChar4 = new Characteristics() { Id = 4, Type = "Garage" };
            dbContext.Characteristics.InsertOneAsync(newChar4);

            var newChar5 = new Characteristics() { Id = 5, Type = "Zwembad" };
            dbContext.Characteristics.InsertOneAsync(newChar5);

            var newChar6 = new Characteristics() { Id = 6, Type = "Carport" };
            dbContext.Characteristics.InsertOneAsync(newChar6);

            var newChar7 = new Characteristics() { Id = 7, Type = "Tennisplein" };
            dbContext.Characteristics.InsertOneAsync(newChar7);

            var newChar8 = new Characteristics() { Id = 8, Type = "Paardenstallen" };
            dbContext.Characteristics.InsertOneAsync(newChar8);
        }

        // Method to add values to the BuyersRenters (kopers_huurders) table
        public void DummyBuyersRenters()
        {
            var newBuyerRenter = new BuyersRenters() { Id = 1, Name = "Longo", Surname = "Lorenzo" };
            dbContext.BuyersRenters.InsertOneAsync(newBuyerRenter);

            var newBuyerRenter2 = new BuyersRenters() { Id = 2, Name = "Fransen", Surname = "Frans" };
            dbContext.BuyersRenters.InsertOneAsync(newBuyerRenter2);

            var newBuyerRenter3 = new BuyersRenters() { Id = 3, Name = "Duitsen", Surname = "Duits" };
            dbContext.BuyersRenters.InsertOneAsync(newBuyerRenter3);
        }

        // Method to add values to the Agreements inherited tables (overeenkomsten - huurovereenkomsten - verkoopsovereenkomsten)
        public void DummyAgreements()
        {
            var newAgreement = new RentAgreements() { Id = 1, Date = DateTime.Today, Guarantee = 3000.51, MonthlyCost = 659.99, BuyersRenters = dbContext.BuyersRenters.Find(s => s.Id == 1).FirstOrDefault<BuyersRenters>(), Properties = dbContext.Properties.Find(s => s.Id == 2).FirstOrDefault<Properties>() };
            dbContext.Agreements.InsertOneAsync(newAgreement);

            var newAgreement2 = new RentAgreements() { Id = 2, Date = DateTime.Today, Guarantee = 1500.00, MonthlyCost = 550.00, BuyersRenters = dbContext.BuyersRenters.Find(s => s.Id == 3).FirstOrDefault<BuyersRenters>(), Properties = dbContext.Properties.Find(s => s.Id == 6).FirstOrDefault<Properties>() };
            dbContext.Agreements.InsertOneAsync(newAgreement2);

            var newAgreement3 = new BuyAgreements() { Id = 3, Date = DateTime.Today, SellingPrice = 255000.00, BuyersRenters = dbContext.BuyersRenters.Find(s => s.Id == 2).FirstOrDefault<BuyersRenters>(), Properties = dbContext.Properties.Find(s => s.Id == 4).FirstOrDefault<Properties>() };
            dbContext.Agreements.InsertOneAsync(newAgreement3);
        }

        //// Method to connect the many-to-many Characteristics and Buildings values (eigenschappen - panden)
        //public void ConnectBuildingsChars()
        //{

        //        //var newchar = dbContext.Characteristics.Where(s => s.Type == "Tuin").FirstOrDefault<Characteristics>();
        //        //var newchar2 = dbContext.Characteristics.Where(s => s.Type == "Kelder").FirstOrDefault<Characteristics>();
        //        //var newchar3 = dbContext.Characteristics.Where(s => s.Type == "Zolder").FirstOrDefault<Characteristics>();
        //        //var newchar4 = dbContext.Characteristics.Where(s => s.Type == "Garage").FirstOrDefault<Characteristics>();
        //        //var newchar5 = dbContext.Characteristics.Where(s => s.Type == "Zwembad").FirstOrDefault<Characteristics>();
        //        //var newchar6 = dbContext.Characteristics.Where(s => s.Type == "Carport").FirstOrDefault<Characteristics>();
        //        //var newchar7 = dbContext.Characteristics.Where(s => s.Type == "Tennisplein").FirstOrDefault<Characteristics>();
        //        //var newchar8 = dbContext.Characteristics.Where(s => s.Type == "Paardenstallen").FirstOrDefault<Characteristics>();

        //        //var newProp = dbContext.Buildings.Where(s => s.Id == 2).FirstOrDefault<Buildings>();
        //        //var newProp2 = dbContext.Buildings.Where(s => s.Id == 3).FirstOrDefault<Buildings>();
        //        //var newProp3 = dbContext.Buildings.Where(s => s.Id == 6).FirstOrDefault<Buildings>();

        //        //unitOfWork.BuildingsRepo.Update(newProp);
        //        //unitOfWork.BuildingsRepo.Update(newProp2);
        //        //unitOfWork.BuildingsRepo.Update(newProp3);

        //        newchar.Buildings.Add(newProp);
        //        newchar.Buildings.Add(newProp2);

        //        newchar2.Buildings.Add(newProp3);

        //        newchar3.Buildings.Add(newProp);
        //        newchar3.Buildings.Add(newProp3);

        //        newchar4.Buildings.Add(newProp);
        //        newchar4.Buildings.Add(newProp3);

        //        newchar5.Buildings.Add(newProp3);

        //        newchar6.Buildings.Add(newProp2);

        //        newchar7.Buildings.Add(newProp3);

        //        newchar8.Buildings.Add(newProp3);

        //        unitOfWork.Commit();

        //}
    }
}
