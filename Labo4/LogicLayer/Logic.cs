﻿using DataLayer.Enumerations;
using DataLayer.Model_Classes;
using DataLayer.Patterns;
using Globals.Interfaces;
using MongoDB.Driver;
using System;
using System.Collections.Generic;
using System.Linq;

namespace LogicLayer
{
    public class Logic : ILogic
    {
        private int counter;
        private MongoDBContext context;

        public Logic(MongoDBContext context)
        {
            this.context = context;
        }


        // ManageCustomersForm methods 

        // Method to get all of the buyers and renters inside of the buyers_renters (kopers_huurders) table
        // Returns list of all the customers
        public List<string> GetCustomers()
        {
            List<string> customers = new List<string>();
            foreach (BuyersRenters br in context.BuyersRenters.Find(br => br.Id != null).ToList())
            {
                customers.Add(br.Id + " " + br.Surname + " " + br.Name);
            }
            return customers;
        }

        // Method which adds the in-form inserted values into the buyers_renters (kopers_huurders) table
        public void AddCustomer(string surname, string name)
        {
            counter = CheckIfIDExists();
            context.BuyersRenters.InsertOneAsync(new BuyersRenters() { Id = counter + 1, Name = name, Surname = surname });
        }

        // Method which deletes the in-form inserted ID from the buyers_renters (kopers_huurders) table
        public void DeleteCustomer(int id)
        {
            context.BuyersRenters.DeleteOneAsync(br => br.Id == id);
        }

        // Method which checks if the inserted ID to delete a certain costumer exists
        public int CheckIfIDExists()
        {
            var getID = (from c in context.BuyersRenters.AsQueryable<BuyersRenters>() orderby c.Id descending select c).FirstOrDefault<BuyersRenters>();
            var checkID = getID.Id;

            return checkID;
        }


        // ManagePropertiesForm methods

        // Method which gets and returns all of the ground types inside the GroundTypes enum
        public List<string> GetGroundTypes()
        {
            List<string> types = new List<string>();
            foreach (GroundTypes ground in Enum.GetValues(typeof(GroundTypes)))
            {
                types.Add(ground.ToString());
            }
            return types;
        }

        // Method which gets and returns all of the building types inside the BuildingsTypes enum
        public List<string> GetBuildingTypes()
        {
            List<string> types = new List<string>();
            foreach (BuildingTypes building in Enum.GetValues(typeof(BuildingTypes)))
            {
                types.Add(building.ToString());
            }
            return types;
        }

        // Gets the highest ID value of the properties collection
        public int GetMaxPropertyID()
        {
            var getID = (from c in context.Properties.AsQueryable<Properties>() orderby c.Id descending select c).FirstOrDefault<Properties>();
            var checkID = getID.Id;

            return checkID;
        }

        // Method to insert values in the building table
        public void AddBuilding(string town, string street, object type, int houseNr)
        {
            counter = GetMaxPropertyID();
            var types = type.ToString();
            context.Properties.InsertOneAsync(new Buildings() { Id = counter + 1, Town = town, Street = street, Types = (BuildingTypes)Enum.Parse(typeof(BuildingTypes), types), HouseNr = houseNr });
        }

        // Method to insert values in the building table
        public void AddBuilding(string town, string street, object type, int boxOrRooms, int houseNr, bool roomsOrBoxNr)
        {
            counter = GetMaxPropertyID();
            var types = type.ToString();
            if (roomsOrBoxNr == true)
                context.Properties.InsertOneAsync(new Buildings() { Id = counter + 1, Town = town, Street = street, Types = (BuildingTypes)Enum.Parse(typeof(BuildingTypes), types), Rooms = boxOrRooms, HouseNr = houseNr });
            else
                context.Properties.InsertOneAsync(new Buildings() { Id = counter + 1, Town = town, Street = street, Types = (BuildingTypes)Enum.Parse(typeof(BuildingTypes), types), HouseNr = houseNr, BoxNr = boxOrRooms });
        }

        // Method to insert values in the building table
        public void AddBuilding(string town, string street, object type, int houseNr, double epc)
        {
            counter = GetMaxPropertyID();
            var types = type.ToString();
            context.Properties.InsertOneAsync(new Buildings() { Id = counter + 1, Town = town, Street = street, Types = (BuildingTypes)Enum.Parse(typeof(BuildingTypes), types), HouseNr = houseNr, EPC = epc });
        }

        // Method to insert values in the building table
        public void AddBuilding(string town, string street, object type, int boxOrRooms, int houseNr, double epc, bool roomsOrBoxNr)
        {
            counter = GetMaxPropertyID();
            var types = type.ToString();
            if (roomsOrBoxNr == true)
                context.Properties.InsertOneAsync(new Buildings() { Id = counter + 1, Town = town, Street = street, Types = (BuildingTypes)Enum.Parse(typeof(BuildingTypes), types), Rooms = boxOrRooms, HouseNr = houseNr, EPC = epc });
            else
                context.Properties.InsertOneAsync(new Buildings() { Id = counter + 1, Town = town, Street = street, Types = (BuildingTypes)Enum.Parse(typeof(BuildingTypes), types), HouseNr = houseNr, BoxNr = boxOrRooms, EPC = epc });
        }

        // Method to insert values in the building table
        public void AddBuilding(string town, string street, object type, int rooms, int houseNr, int boxNr)
        {
            counter = GetMaxPropertyID();
            var types = type.ToString();
            context.Properties.InsertOneAsync(new Buildings() { Id = counter + 1, Town = town, Street = street, Types = (BuildingTypes)Enum.Parse(typeof(BuildingTypes), types), Rooms = rooms, HouseNr = houseNr, BoxNr = boxNr });
        }

        // Method to insert values in the building table
        public void AddBuilding(string town, string street, object type, int rooms, int houseNr, int boxNr, double epc)
        {
            counter = GetMaxPropertyID();
            var types = type.ToString();
            context.Properties.InsertOneAsync(new Buildings() { Id = counter + 1, Town = town, Street = street, Types = (BuildingTypes)Enum.Parse(typeof(BuildingTypes), types), Rooms = rooms, HouseNr = houseNr, BoxNr = boxNr, EPC = epc });
        }

        // Method to insert values into the grounds table
        public void AddGround(string town, string street, object type)
        {
            counter = GetMaxPropertyID();
            var types = type.ToString();
            context.Properties.InsertOneAsync(new Grounds() { Id = counter + 1, Town = town, Street = street, Types = (GroundTypes)Enum.Parse(typeof(GroundTypes), types) });
        }

        // Method to insert values into the grounds table
        public void AddGround(string town, string street, double surface, object type)
        {
            counter = GetMaxPropertyID();
            var types = type.ToString();
            context.Properties.InsertOneAsync(new Grounds() { Id = counter + 1, Town = town, Street = street, Surface = surface, Types = (GroundTypes)Enum.Parse(typeof(GroundTypes), types) });
        }



        // ManageCharacteristicsForm methods

        // Gets and returns all of the characteristics inside the Characteristics (eigenschappen) table
        public List<string> GetCharacteristics()
        {
            List<string> characteristics = new List<string>();
            foreach (Characteristics chars in context.Characteristics.Find(chars => chars.Id != null).ToList())
            {
                characteristics.Add(chars.Type);
            }
            return characteristics;
        }

        // Gets and returns all of the properties inside the Properties (eigendommen) table
        public List<string> GetProperties()
        {
            List<string> properties = new List<string>();
            foreach (Buildings building in context.Buildings.Find(build => build.Id != null).ToList())
            {
                properties.Add(building.Id + " " + building.Street + " " + building.Town + " " + building.HouseNr);
            }
            return properties;
        }

        // Gets and sets all the Charteristics that are connected with the specific building
        public List<int> GetCheckedChars(int selectedId)
        {

            List<int> checkedChar = new List<int>();
            foreach (ICollection<Characteristics> v in context.Buildings.Find(id => id.Id == selectedId).ToList())
            {
                foreach (var c in v.Where(g => g.Id != null).Select(id => id.Id).ToList())
                {
                    int vs = c;
                    checkedChar.Add(vs);
                }
            }
            return checkedChar;
        }

        // Method to update the checked or unchecked Characteristics for a certain building
        public void UpdateCheckedChars(int selectedCharIndex, int propertyID, bool checkedUnchecked)
        {
            // Check if checked or unchecked 
            if (checkedUnchecked == true)
            {
                foreach (var building in context.Buildings.AsQueryable().ToList())
                {
                    if (building.Id == propertyID)
                    {
                        context.Buildings.UpdateOneAsync(x => x.Id == propertyID, Builders<Buildings>.Update.Set(chars => chars.Characteristics.Id, selectedCharIndex));
                    }
                }
            }
            else if (checkedUnchecked == false)
            {
                context.Buildings.DeleteOneAsync(chars => chars.Characteristics.Id == selectedCharIndex);
            }
        }


        // ManageAgreementsForm methods

        // Gets all the properties that don't have an agreement yet
        public List<string> GetNoAgreements()
        {
            var propIDs = context.Properties.AsQueryable().ToList();
            var agreementIDs = context.Agreements.AsQueryable().ToList();

            List<Properties> allProperties = new List<Properties>();
            List<Properties> matchingProperties = new List<Properties>();

            // Fill all properties list and fill properties list with properties that match with the ID inside the Agreements table
            foreach (var propID in propIDs)
            {
                allProperties.Add(propID);
                foreach (var agreeID in agreementIDs)
                {
                    if (agreeID.Properties != null)
                    {
                        if (agreeID.Properties.Id == agreeID.Id) matchingProperties.Add(propID);
                    }
                }
            }

            // Remove all of the matching values out of the allProperties list
            allProperties.RemoveAll(x => matchingProperties.Exists(y => y.Id == x.Id && y.Street == y.Street && x.Town == x.Town));
            List<string> noAgreement = new List<string>();

            foreach (Properties pr in allProperties)
            {
                noAgreement.Add(pr.Id.ToString() + " " + pr.Street.ToString() + " " + pr.Town.ToString());
            }

            return noAgreement;
        }

        // Method which adds a new Sell agreement to the Agreements table
        public void AddSellAgreement(DateTime date, int property, int buyerRenter, double price)
        {
            counter = GetMaxAgreementID();
            var prop = context.Properties.Find(pr => pr.Id == property).ToList();
            var buyRenter = context.BuyersRenters.Find(br => br.Id == buyerRenter).ToList();

            context.Agreements.InsertOneAsync(new BuyAgreements() { Id = counter + 1, Date = date, Properties = prop.FirstOrDefault(), BuyersRenters = buyRenter.FirstOrDefault(), SellingPrice = price });
        }

        // Method which adds a new Rent agreement to the Agreements table
        public void AddRentAgreement(DateTime date, int property, int buyerRenter, double guarantee, double monthlyPayment)
        {
            counter = GetMaxAgreementID();
            var prop = context.Properties.Find(pr => pr.Id == property).ToList();
            var buyRenter = context.BuyersRenters.Find(br => br.Id == buyerRenter).ToList();

            context.Agreements.InsertOneAsync(new RentAgreements() { Id = counter + 1, Date = date, Properties = prop.FirstOrDefault(), BuyersRenters = buyRenter.FirstOrDefault(), Guarantee = guarantee, MonthlyCost = monthlyPayment });
        }

        // Method which adds a new Rent agreement to the Agreements table
        public void AddRentAgreement(DateTime date, int property, int buyerRenter, double baseCosts, double guarantee, double monthlyPayment)
        {
            counter = GetMaxAgreementID();
            var prop = context.Properties.Find(pr => pr.Id == property).ToList();
            var buyRenter = context.BuyersRenters.Find(br => br.Id == buyerRenter).ToList();

            context.Agreements.InsertOneAsync(new RentAgreements() { Id = counter + 1, Date = date, Properties = prop.FirstOrDefault(), BuyersRenters = buyRenter.FirstOrDefault(), FixedCosts = baseCosts, Guarantee = guarantee, MonthlyCost = monthlyPayment });
        }

        // Gets the highest ID value of the properties collection
        public int GetMaxAgreementID()
        {
            var getID = (from c in context.Agreements.AsQueryable<Agreements>() orderby c.Id descending select c).FirstOrDefault<Agreements>();
            var checkID = getID.Id;

            return checkID;
        }



        // StopAgreementsForm methods

        // Method to get all of the properties that have an agreement
        public List<string> GetAgreements()
        {
            var propIDs = context.Properties.AsQueryable().ToList();
            var agreementIDs = context.Agreements.AsQueryable().ToList();

            List<string> properties = new List<string>();

            foreach (var propID in propIDs)
            {
                foreach (var agreeID in agreementIDs)
                {
                    if (agreeID.Properties != null)
                    {
                        if (agreeID.Properties.Id.ToString() == propID.Id.ToString()) properties.Add(propID.Id + " " + propID.Town + " " + propID.Street);
                    }
                }
            }
            return properties;
        }

        // Method to terminate an agreement of a property
        public void TerminateAgreement(int propertyID)
        {
            int id = 0;
            var agreement = context.Agreements.Find(agree => agree.Properties.Id == propertyID).ToList();

            foreach (Agreements ag in agreement)
            {
                id = ag.Id;
            }

            context.Agreements.DeleteOneAsync(getId => getId.Id == propertyID);

        }

        // Method to decide if a property is a ground of a building
        public bool GroundOrBuilding(int propertyID)
        {
            var buildings = context.Buildings.AsQueryable().ToList();

            foreach (Buildings building in buildings)
            {
                if (propertyID == building.Id) return true;
            }

            return false;
        }
    }
}
