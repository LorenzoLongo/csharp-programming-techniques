﻿namespace MainForm
{
    partial class OverviewForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.btnAgreements = new System.Windows.Forms.Button();
            this.btnCharacteristics = new System.Windows.Forms.Button();
            this.btnProperties = new System.Windows.Forms.Button();
            this.btnCustomers = new System.Windows.Forms.Button();
            this.SuspendLayout();
            // 
            // btnAgreements
            // 
            this.btnAgreements.Location = new System.Drawing.Point(16, 193);
            this.btnAgreements.Name = "btnAgreements";
            this.btnAgreements.Size = new System.Drawing.Size(252, 23);
            this.btnAgreements.TabIndex = 7;
            this.btnAgreements.Text = "Overeenkomsten beheren";
            this.btnAgreements.UseVisualStyleBackColor = true;
            this.btnAgreements.Click += new System.EventHandler(this.btnAgreements_Click_1);
            // 
            // btnCharacteristics
            // 
            this.btnCharacteristics.Location = new System.Drawing.Point(16, 145);
            this.btnCharacteristics.Name = "btnCharacteristics";
            this.btnCharacteristics.Size = new System.Drawing.Size(252, 23);
            this.btnCharacteristics.TabIndex = 6;
            this.btnCharacteristics.Text = "Eigenschappen van panden beheren";
            this.btnCharacteristics.UseVisualStyleBackColor = true;
            this.btnCharacteristics.Click += new System.EventHandler(this.btnCharacteristics_Click_1);
            // 
            // btnProperties
            // 
            this.btnProperties.Location = new System.Drawing.Point(16, 97);
            this.btnProperties.Name = "btnProperties";
            this.btnProperties.Size = new System.Drawing.Size(252, 23);
            this.btnProperties.TabIndex = 5;
            this.btnProperties.Text = "Eigendommen beheren";
            this.btnProperties.UseVisualStyleBackColor = true;
            this.btnProperties.Click += new System.EventHandler(this.btnProperties_Click_1);
            // 
            // btnCustomers
            // 
            this.btnCustomers.Location = new System.Drawing.Point(16, 44);
            this.btnCustomers.Name = "btnCustomers";
            this.btnCustomers.Size = new System.Drawing.Size(252, 23);
            this.btnCustomers.TabIndex = 4;
            this.btnCustomers.Text = "Klanten beheren";
            this.btnCustomers.UseVisualStyleBackColor = true;
            this.btnCustomers.Click += new System.EventHandler(this.btnCustomers_Click_1);
            // 
            // OverviewForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(284, 261);
            this.Controls.Add(this.btnAgreements);
            this.Controls.Add(this.btnCharacteristics);
            this.Controls.Add(this.btnProperties);
            this.Controls.Add(this.btnCustomers);
            this.Name = "OverviewForm";
            this.Text = "Overzicht";
            this.FormClosed += new System.Windows.Forms.FormClosedEventHandler(this.OverviewForm_FormClosed_1);
            this.Load += new System.EventHandler(this.Form1_Load);
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Button btnAgreements;
        private System.Windows.Forms.Button btnCharacteristics;
        private System.Windows.Forms.Button btnProperties;
        private System.Windows.Forms.Button btnCustomers;
    }
}

