﻿using Globals.Interfaces;
using System;
using System.Drawing;
using System.Windows.Forms;

namespace MainForm
{
    public partial class ManageAgreementsForm : Form
    {
        private readonly ILogic logic;

        public ManageAgreementsForm(ILogic logic)
        {
            this.logic = logic;
            InitializeComponent();
        }

        private void ManageAgreementsForm_Load(object sender, EventArgs e)
        {
            if (radioBtnSellAgreement.Checked) groupBoxRent.Enabled = false;
            comboBoxProperties.DataSource = logic.GetNoAgreements();
            comboBoxBuyersRenters.DataSource = logic.GetCustomers();
        }

        private void ManageAgreementsForm_FormClosed(object sender, FormClosedEventArgs e)
        {
            OverviewForm overview = new OverviewForm(logic);
            this.Hide();
            this.Close();
            overview.ShowDialog();
        }

        private void radioBtnSellAgreement_CheckedChanged(object sender, EventArgs e)
        {
            groupBoxSell.Enabled = true;
            groupBoxRent.Enabled = false;
            lblErrorRent.Text = lblSucceed.Text = "";
        }

        private void radioBtnRentAgreement_CheckedChanged(object sender, EventArgs e)
        {
            groupBoxRent.Enabled = true;
            groupBoxSell.Enabled = false;
            lblErrorSell.Text = lblSucceed.Text = "";
        }

        private void btnAddAgreement_Click(object sender, EventArgs e)
        {
            if (radioBtnSellAgreement.Checked == true) NewSellAgreement();
            else if (radioBtnRentAgreement.Checked == true) NewRentAgreement();
        }

        private void NewSellAgreement()
        {
            if (string.IsNullOrWhiteSpace(textBoxSellPrice.Text))
            {
                lblErrorSell.Text = "Vul het veld verkoopprijs in!";
                lblErrorSell.ForeColor = Color.Red;
            }
            else
            {
                lblSucceed.Text = lblErrorSell.Text = lblErrorRent.Text = "";
                lblSucceed.Text = "Verkoopsovereenkomst is afgesloten!";
                lblSucceed.ForeColor = Color.Green;
                SellAgreement();
            }
        }
        private void NewRentAgreement()
        {
            if (string.IsNullOrWhiteSpace(textBoxGuarantee.Text) || string.IsNullOrWhiteSpace(textBoxMonthlyPayment.Text))
            {
                lblErrorRent.Text = "Vul de velden waarborg en maandbedrag in!";
                lblErrorRent.ForeColor = Color.Red;
            }
            else
            {
                lblSucceed.Text = lblErrorSell.Text = lblErrorRent.Text = "";
                lblSucceed.Text = "Huurovereenkomst is afgesloten!!";
                lblSucceed.ForeColor = Color.Green;
                OverloadedRentAgreement();
            }
        }

        private void OverloadedRentAgreement()
        {
            if (string.IsNullOrEmpty(textBoxCosts.Text))
                logic.AddRentAgreement(dateTimePickerDate.Value, GetPropertyID(), GetBuyerRenterID(), double.Parse(textBoxGuarantee.Text), double.Parse(textBoxMonthlyPayment.Text));
            else
                logic.AddRentAgreement(dateTimePickerDate.Value, GetPropertyID(), GetBuyerRenterID(), double.Parse(textBoxCosts.Text), double.Parse(textBoxGuarantee.Text), double.Parse(textBoxMonthlyPayment.Text));

            comboBoxProperties.DataSource = logic.GetNoAgreements();
        }

        private void SellAgreement()
        {
            if (!string.IsNullOrEmpty(textBoxSellPrice.Text)) logic.AddSellAgreement(dateTimePickerDate.Value, GetPropertyID(), GetBuyerRenterID(), double.Parse(textBoxSellPrice.Text));

            comboBoxProperties.DataSource = logic.GetNoAgreements();
        }

        private int GetPropertyID()
        {
            return Int32.Parse(comboBoxProperties.Text.Substring(0, 2));
        }

        private int GetBuyerRenterID()
        {
            return Int32.Parse(comboBoxBuyersRenters.Text.Substring(0, 2));
        }

        private void btnStopAgreement_Click_1(object sender, EventArgs e)
        {
            StopAgreementForm stop = new StopAgreementForm(logic);
            this.Hide();
            this.Close();
            stop.ShowDialog();
        }
    }
}
