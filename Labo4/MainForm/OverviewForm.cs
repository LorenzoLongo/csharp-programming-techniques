﻿using Globals.Interfaces;
using System;
using System.Windows.Forms;

namespace MainForm
{
    public partial class OverviewForm : Form
    {
        private readonly ILogic logic;
        private readonly IDummyValues values;

        // Constructor
        public OverviewForm(ILogic logic)
        {
            this.logic = logic;
            InitializeComponent();
        }

        // Constructor overload
        public OverviewForm(ILogic logic, IDummyValues values)
        {
            this.logic = logic;
            this.values = values;
            InitializeComponent();
        }

        private void Form1_Load(object sender, EventArgs e)
        {
            // EENMALIG RUNNEN MET DEZE METHODEN (DIENT ENKEL VOOR HET OPVULLEN VAN DE DATABASE)
            values.DummyProperties();
            values.DummyCharacteristics();
            values.DummyBuyersRenters();
            values.DummyAgreements();
        }

        // Method to open the Customers Form
        private void btnCustomers_Click_1(object sender, EventArgs e)
        {
            this.Hide();
            ManageCustomersForm customers = new ManageCustomersForm(logic);
            customers.ShowDialog();
        }

        // Method to close the application
        private void OverviewForm_FormClosed_1(object sender, FormClosedEventArgs e)
        {
            Application.Exit();
        }

        // Method to open the Properties Form
        private void btnProperties_Click_1(object sender, EventArgs e)
        {
            this.Hide();
            ManagePropertiesForm properties = new ManagePropertiesForm(logic);
            properties.ShowDialog();
        }

        // Method to open the Characteristics Form
        private void btnCharacteristics_Click_1(object sender, EventArgs e)
        {
            this.Hide();
            ManageCharacteristics chars = new ManageCharacteristics(logic);
            chars.ShowDialog();
        }

        // Method to open the Agreements Form
        private void btnAgreements_Click_1(object sender, EventArgs e)
        {
            this.Hide();
            ManageAgreementsForm agreements = new ManageAgreementsForm(logic);
            agreements.ShowDialog();
        }
    }
}
