﻿namespace MainForm
{
    partial class ManageCharacteristics
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.comboBoxProperties = new System.Windows.Forms.ComboBox();
            this.btnOK = new System.Windows.Forms.Button();
            this.checkedListBoxChars = new System.Windows.Forms.CheckedListBox();
            this.lblChars = new System.Windows.Forms.Label();
            this.lblProperties = new System.Windows.Forms.Label();
            this.SuspendLayout();
            // 
            // comboBoxProperties
            // 
            this.comboBoxProperties.FormattingEnabled = true;
            this.comboBoxProperties.Location = new System.Drawing.Point(124, 24);
            this.comboBoxProperties.Name = "comboBoxProperties";
            this.comboBoxProperties.Size = new System.Drawing.Size(158, 21);
            this.comboBoxProperties.TabIndex = 9;
            this.comboBoxProperties.SelectedIndexChanged += new System.EventHandler(this.comboBoxProperties_SelectedIndexChanged_1);
            // 
            // btnOK
            // 
            this.btnOK.Location = new System.Drawing.Point(333, 277);
            this.btnOK.Name = "btnOK";
            this.btnOK.Size = new System.Drawing.Size(75, 23);
            this.btnOK.TabIndex = 8;
            this.btnOK.Text = "OK";
            this.btnOK.UseVisualStyleBackColor = true;
            this.btnOK.Click += new System.EventHandler(this.btnOK_Click_1);
            // 
            // checkedListBoxChars
            // 
            this.checkedListBoxChars.FormattingEnabled = true;
            this.checkedListBoxChars.Location = new System.Drawing.Point(124, 86);
            this.checkedListBoxChars.Name = "checkedListBoxChars";
            this.checkedListBoxChars.Size = new System.Drawing.Size(158, 214);
            this.checkedListBoxChars.TabIndex = 7;
            this.checkedListBoxChars.SelectedValueChanged += new System.EventHandler(this.checkedListBoxChars_SelectedValueChanged_1);
            // 
            // lblChars
            // 
            this.lblChars.AutoSize = true;
            this.lblChars.Location = new System.Drawing.Point(15, 86);
            this.lblChars.Name = "lblChars";
            this.lblChars.Size = new System.Drawing.Size(81, 13);
            this.lblChars.TabIndex = 6;
            this.lblChars.Text = "Eigenschappen";
            // 
            // lblProperties
            // 
            this.lblProperties.AutoSize = true;
            this.lblProperties.Location = new System.Drawing.Point(15, 27);
            this.lblProperties.Name = "lblProperties";
            this.lblProperties.Size = new System.Drawing.Size(44, 13);
            this.lblProperties.TabIndex = 5;
            this.lblProperties.Text = "Panden";
            // 
            // ManageCharacteristics
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(424, 328);
            this.Controls.Add(this.comboBoxProperties);
            this.Controls.Add(this.btnOK);
            this.Controls.Add(this.checkedListBoxChars);
            this.Controls.Add(this.lblChars);
            this.Controls.Add(this.lblProperties);
            this.Name = "ManageCharacteristics";
            this.Text = "Eigenschappen van panden beheren";
            this.FormClosed += new System.Windows.Forms.FormClosedEventHandler(this.ManageCharacteristics_FormClosed);
            this.Load += new System.EventHandler(this.ManageCharacteristics_Load);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.ComboBox comboBoxProperties;
        private System.Windows.Forms.Button btnOK;
        private System.Windows.Forms.CheckedListBox checkedListBoxChars;
        private System.Windows.Forms.Label lblChars;
        private System.Windows.Forms.Label lblProperties;
    }
}