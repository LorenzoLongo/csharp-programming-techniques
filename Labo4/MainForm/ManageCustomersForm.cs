﻿using Globals.Interfaces;
using System;
using System.Drawing;
using System.Windows.Forms;

namespace MainForm
{
    public partial class ManageCustomersForm : Form
    {
        private readonly ILogic logic;

        public ManageCustomersForm(ILogic logic)
        {
            this.logic = logic;
            InitializeComponent();
        }

        private void ManageCustomersForm_Load(object sender, EventArgs e)
        {
            comboBoxCustomers.DataSource = logic.GetCustomers();
        }

        private void btnDeleteCust_Click_1(object sender, EventArgs e)
        {
            if (string.IsNullOrWhiteSpace(txtBoxDeleteCustID.Text) || Int32.Parse(txtBoxDeleteCustID.Text) > logic.CheckIfIDExists())
            {
                lblErrorDelCust.Text = "Geef een geldig ID in!";
                lblErrorDelCust.ForeColor = Color.Red;
            }
            else
            {
                logic.DeleteCustomer(Int32.Parse(txtBoxDeleteCustID.Text));

                lblErrorDelCust.Text = "Klant succesvol verwijderd!";
                lblErrorDelCust.ForeColor = Color.Green;
                comboBoxCustomers.DataSource = logic.GetCustomers();
            }
        }

        private void btnAddCust_Click_1(object sender, EventArgs e)
        {
            if (string.IsNullOrWhiteSpace(txtBoxName.Text) && string.IsNullOrWhiteSpace(txtBoxSurname.Text))
            {
                lblErrorAddCust.Text = "Geef een geldige naam of voornaam in!";
                lblErrorAddCust.ForeColor = Color.Red;
            }
            else
            {
                logic.AddCustomer(txtBoxSurname.Text, txtBoxName.Text);

                lblErrorAddCust.Text = "Klant succesvol toegevoegd!";
                lblErrorAddCust.ForeColor = Color.Green;
                comboBoxCustomers.DataSource = logic.GetCustomers();
            }
        }

        private void ManageCustomersForm_FormClosed_1(object sender, FormClosedEventArgs e)
        {
            OverviewForm overview = new OverviewForm(logic);
            this.Hide();
            this.Close();
            overview.ShowDialog();
        }
    }
}
