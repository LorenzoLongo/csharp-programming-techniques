﻿using Globals.Interfaces;
using System;
using System.Windows.Forms;

namespace MainForm
{
    public partial class ManageCharacteristics : Form
    {
        private readonly ILogic logic;

        public ManageCharacteristics(ILogic logic)
        {
            this.logic = logic;
            InitializeComponent();
        }

        private void ManageCharacteristics_Load(object sender, EventArgs e)
        {
            foreach (string v in logic.GetCharacteristics())
            {
                checkedListBoxChars.Items.Add(v);
            }
            comboBoxProperties.DataSource = logic.GetProperties();
        }

        private void ManageCharacteristics_FormClosed(object sender, FormClosedEventArgs e)
        {
            OverviewForm overview = new OverviewForm(logic);
            this.Hide();
            this.Close();
            overview.ShowDialog();
        }

        private int GetBuildingID()
        {
            return Int32.Parse(comboBoxProperties.Text.Substring(0, 2));
        }

        private void comboBoxProperties_SelectedIndexChanged_1(object sender, EventArgs e)
        {
            for (int i = 0; i < checkedListBoxChars.Items.Count; i++)
            {
                checkedListBoxChars.SetItemChecked(i, false);
            }
            foreach (int v in logic.GetCheckedChars(GetBuildingID()))
            {
                checkedListBoxChars.SetItemChecked((v - 1), true);
            }
        }

        private void checkedListBoxChars_SelectedValueChanged_1(object sender, EventArgs e)
        {
            var status = checkedListBoxChars.GetItemCheckState(checkedListBoxChars.SelectedIndex);
            if (status.ToString() == "Checked") logic.UpdateCheckedChars(checkedListBoxChars.SelectedIndex, GetBuildingID(), true);
            else if (status.ToString() == "Unchecked") logic.UpdateCheckedChars(checkedListBoxChars.SelectedIndex, GetBuildingID(), false);
        }

        private void btnOK_Click_1(object sender, EventArgs e)
        {
            OverviewForm overview = new OverviewForm(logic);
            this.Hide();
            this.Close();
            overview.ShowDialog();
        }
    }
}
