﻿using DataLayer.Patterns;
using Globals.Interfaces;
using LogicLayer;
using System;
using System.Windows.Forms;

namespace MainForm
{
    static class Program
    {
        /// <summary>
        /// The main entry point for the application.
        /// </summary>
        [STAThread]
        static void Main()
        {
            MongoDBContext context = new MongoDBContext("mongodb://admin:Azerty123@ds233320.mlab.com:33320/admin-labo4db");
            MongoDBContext context2 = new MongoDBContext("mongodb://localhost");
            IDummyValues values = new DummyValues(context);
            ILogic logic = new Logic(context);
            Application.EnableVisualStyles();
            Application.SetCompatibleTextRenderingDefault(false);
            Application.Run(new OverviewForm(logic, values));
        }
    }
}
