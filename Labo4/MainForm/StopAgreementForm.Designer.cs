﻿namespace MainForm
{
    partial class StopAgreementForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.lblSuceed = new System.Windows.Forms.Label();
            this.btnStopAgreement = new System.Windows.Forms.Button();
            this.groupBoxTypes = new System.Windows.Forms.GroupBox();
            this.radioBtnGrounds = new System.Windows.Forms.RadioButton();
            this.radioBtnBuildings = new System.Windows.Forms.RadioButton();
            this.comboBoxProperties = new System.Windows.Forms.ComboBox();
            this.lblProperties = new System.Windows.Forms.Label();
            this.groupBoxTypes.SuspendLayout();
            this.SuspendLayout();
            // 
            // lblSuceed
            // 
            this.lblSuceed.AutoSize = true;
            this.lblSuceed.Location = new System.Drawing.Point(102, 163);
            this.lblSuceed.Name = "lblSuceed";
            this.lblSuceed.Size = new System.Drawing.Size(0, 13);
            this.lblSuceed.TabIndex = 9;
            // 
            // btnStopAgreement
            // 
            this.btnStopAgreement.Location = new System.Drawing.Point(268, 158);
            this.btnStopAgreement.Name = "btnStopAgreement";
            this.btnStopAgreement.Size = new System.Drawing.Size(75, 23);
            this.btnStopAgreement.TabIndex = 8;
            this.btnStopAgreement.Text = "Beëindigen";
            this.btnStopAgreement.UseVisualStyleBackColor = true;
            this.btnStopAgreement.Click += new System.EventHandler(this.btnStopAgreement_Click);
            // 
            // groupBoxTypes
            // 
            this.groupBoxTypes.Controls.Add(this.radioBtnGrounds);
            this.groupBoxTypes.Controls.Add(this.radioBtnBuildings);
            this.groupBoxTypes.Location = new System.Drawing.Point(12, 65);
            this.groupBoxTypes.Name = "groupBoxTypes";
            this.groupBoxTypes.Size = new System.Drawing.Size(331, 74);
            this.groupBoxTypes.TabIndex = 7;
            this.groupBoxTypes.TabStop = false;
            // 
            // radioBtnGrounds
            // 
            this.radioBtnGrounds.AutoSize = true;
            this.radioBtnGrounds.Location = new System.Drawing.Point(17, 19);
            this.radioBtnGrounds.Name = "radioBtnGrounds";
            this.radioBtnGrounds.Size = new System.Drawing.Size(54, 17);
            this.radioBtnGrounds.TabIndex = 1;
            this.radioBtnGrounds.TabStop = true;
            this.radioBtnGrounds.Text = "Grond";
            this.radioBtnGrounds.UseVisualStyleBackColor = true;
            // 
            // radioBtnBuildings
            // 
            this.radioBtnBuildings.AutoSize = true;
            this.radioBtnBuildings.Location = new System.Drawing.Point(256, 19);
            this.radioBtnBuildings.Name = "radioBtnBuildings";
            this.radioBtnBuildings.Size = new System.Drawing.Size(50, 17);
            this.radioBtnBuildings.TabIndex = 0;
            this.radioBtnBuildings.TabStop = true;
            this.radioBtnBuildings.Text = "Pand";
            this.radioBtnBuildings.UseVisualStyleBackColor = true;
            // 
            // comboBoxProperties
            // 
            this.comboBoxProperties.FormattingEnabled = true;
            this.comboBoxProperties.Location = new System.Drawing.Point(105, 20);
            this.comboBoxProperties.Name = "comboBoxProperties";
            this.comboBoxProperties.Size = new System.Drawing.Size(238, 21);
            this.comboBoxProperties.TabIndex = 6;
            this.comboBoxProperties.SelectedIndexChanged += new System.EventHandler(this.comboBoxProperties_SelectedIndexChanged);
            // 
            // lblProperties
            // 
            this.lblProperties.AutoSize = true;
            this.lblProperties.Location = new System.Drawing.Point(9, 23);
            this.lblProperties.Name = "lblProperties";
            this.lblProperties.Size = new System.Drawing.Size(74, 13);
            this.lblProperties.TabIndex = 5;
            this.lblProperties.Text = "Eigendommen";
            // 
            // StopAgreementForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(369, 213);
            this.Controls.Add(this.lblSuceed);
            this.Controls.Add(this.btnStopAgreement);
            this.Controls.Add(this.groupBoxTypes);
            this.Controls.Add(this.comboBoxProperties);
            this.Controls.Add(this.lblProperties);
            this.Name = "StopAgreementForm";
            this.Text = "Bestaande overeenkomsten beëindigen";
            this.FormClosed += new System.Windows.Forms.FormClosedEventHandler(this.StopAgreementForm_FormClosed);
            this.Load += new System.EventHandler(this.StopAgreementForm_Load);
            this.groupBoxTypes.ResumeLayout(false);
            this.groupBoxTypes.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label lblSuceed;
        private System.Windows.Forms.Button btnStopAgreement;
        private System.Windows.Forms.GroupBox groupBoxTypes;
        private System.Windows.Forms.RadioButton radioBtnGrounds;
        private System.Windows.Forms.RadioButton radioBtnBuildings;
        private System.Windows.Forms.ComboBox comboBoxProperties;
        private System.Windows.Forms.Label lblProperties;
    }
}