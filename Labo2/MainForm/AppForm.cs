﻿using Globals;
using Globals.Interfaces;
using Microsoft.VisualBasic;
using System;
using System.Data;
using System.Linq;
using System.Text.RegularExpressions;
using System.Windows.Forms;

namespace MainForm
{
    // Class which contains all of the interaction with the user/form
    public partial class AppForm : Form
    {
        private DateTime time;
        private string playerName, gameName;
        private int score;
        private readonly ILogic logic;
        public AppForm(ILogic logic)
        {
            this.logic = logic;
            do
            {
                playerName = Interaction.InputBox("Geef de naam van de speler in:", "Wat is je naam?");
            } while (playerName == null || playerName == "");
            
            InitializeComponent();
        }

        private void AppForm_Load(object sender, EventArgs e)
        {
            hiLbl.Text = "Dag " + playerName.First().ToString().ToUpper() + playerName.Substring(1);
            dataGridView1.DataSource = logic.SQLiteDS.Tables["Players"];
            dataGridView2.DataSource = logic.MySQLDS.Tables["Games"];
            dataGridView3.DataSource = logic.SQLiteDS.Tables["Games"];
            gamesComboBox.DataSource = logic.GetGamesAsList().Select(l => l.Name).ToList();
        }

        private void scoreLocalBtn_Click(object sender, EventArgs e)
        {
            localScoreListBox.Items.Clear();
            foreach(ScoreItem score in logic.GetTop10Scores(gamesComboBox.Text))
            {
                localScoreListBox.Items.Add(score.ToString());
            }
        }

        private void saveBtn_Click(object sender, EventArgs e)
        {
            bool check = false;
            if(newGameTxtBox.Text == "" || newGameTxtBox.Text == null) gameName = gamesComboBox.Text;
            else gameName = newGameTxtBox.Text;
            if (scoreTxtBox.Text == "" || scoreTxtBox.Text == null) MessageBox.Show("Geef een numerieke waarde in:");
            else if (Regex.IsMatch(scoreTxtBox.Text, "[0-9]"))
            {
                score = Int32.Parse(scoreTxtBox.Text);
                check = true;
            }
            else MessageBox.Show("Geef numerieke waarde in:");
            time = DateTime.Now;
            if (check == true)
            {
                logic.InsertScore(playerName, gameName, score, time);
                dataGridView1.DataSource = logic.SQLiteDS.Tables["Players"];
                dataGridView3.DataSource = logic.SQLiteDS.Tables["Games"];
            }
            
        }
    }
}
