﻿namespace MainForm
{
    partial class AppForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.dataGridView1 = new System.Windows.Forms.DataGridView();
            this.dataGridView2 = new System.Windows.Forms.DataGridView();
            this.gamesComboBox = new System.Windows.Forms.ComboBox();
            this.newGameTxtBox = new System.Windows.Forms.TextBox();
            this.scoreTxtBox = new System.Windows.Forms.TextBox();
            this.selectGameLbl = new System.Windows.Forms.Label();
            this.giveScoreLbl = new System.Windows.Forms.Label();
            this.helloLbl = new System.Windows.Forms.Label();
            this.hiLbl = new System.Windows.Forms.Label();
            this.saveBtn = new System.Windows.Forms.Button();
            this.scoreLocalBtn = new System.Windows.Forms.Button();
            this.localScoreListBox = new System.Windows.Forms.ListBox();
            this.dataGridView3 = new System.Windows.Forms.DataGridView();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridView1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridView2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridView3)).BeginInit();
            this.SuspendLayout();
            // 
            // dataGridView1
            // 
            this.dataGridView1.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dataGridView1.Location = new System.Drawing.Point(12, 12);
            this.dataGridView1.Name = "dataGridView1";
            this.dataGridView1.Size = new System.Drawing.Size(539, 219);
            this.dataGridView1.TabIndex = 1;
            // 
            // dataGridView2
            // 
            this.dataGridView2.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dataGridView2.Location = new System.Drawing.Point(12, 249);
            this.dataGridView2.Name = "dataGridView2";
            this.dataGridView2.Size = new System.Drawing.Size(539, 219);
            this.dataGridView2.TabIndex = 2;
            // 
            // gamesComboBox
            // 
            this.gamesComboBox.FormattingEnabled = true;
            this.gamesComboBox.Location = new System.Drawing.Point(171, 545);
            this.gamesComboBox.Name = "gamesComboBox";
            this.gamesComboBox.Size = new System.Drawing.Size(120, 21);
            this.gamesComboBox.TabIndex = 3;
            // 
            // newGameTxtBox
            // 
            this.newGameTxtBox.Location = new System.Drawing.Point(15, 545);
            this.newGameTxtBox.Name = "newGameTxtBox";
            this.newGameTxtBox.Size = new System.Drawing.Size(120, 20);
            this.newGameTxtBox.TabIndex = 4;
            // 
            // scoreTxtBox
            // 
            this.scoreTxtBox.Location = new System.Drawing.Point(15, 598);
            this.scoreTxtBox.Name = "scoreTxtBox";
            this.scoreTxtBox.Size = new System.Drawing.Size(120, 20);
            this.scoreTxtBox.TabIndex = 5;
            // 
            // selectGameLbl
            // 
            this.selectGameLbl.AutoSize = true;
            this.selectGameLbl.Location = new System.Drawing.Point(12, 519);
            this.selectGameLbl.Name = "selectGameLbl";
            this.selectGameLbl.Size = new System.Drawing.Size(290, 13);
            this.selectGameLbl.TabIndex = 6;
            this.selectGameLbl.Text = "Vul naam van spel in of selecteer een spel uit de combobox:";
            // 
            // giveScoreLbl
            // 
            this.giveScoreLbl.AutoSize = true;
            this.giveScoreLbl.Location = new System.Drawing.Point(12, 582);
            this.giveScoreLbl.Name = "giveScoreLbl";
            this.giveScoreLbl.Size = new System.Drawing.Size(73, 13);
            this.giveScoreLbl.TabIndex = 7;
            this.giveScoreLbl.Text = "Geef score in:";
            // 
            // helloLbl
            // 
            this.helloLbl.AutoSize = true;
            this.helloLbl.Location = new System.Drawing.Point(15, 488);
            this.helloLbl.Name = "helloLbl";
            this.helloLbl.Size = new System.Drawing.Size(0, 13);
            this.helloLbl.TabIndex = 8;
            // 
            // hiLbl
            // 
            this.hiLbl.AutoSize = true;
            this.hiLbl.Location = new System.Drawing.Point(12, 488);
            this.hiLbl.Name = "hiLbl";
            this.hiLbl.Size = new System.Drawing.Size(0, 13);
            this.hiLbl.TabIndex = 9;
            // 
            // saveBtn
            // 
            this.saveBtn.Location = new System.Drawing.Point(12, 624);
            this.saveBtn.Name = "saveBtn";
            this.saveBtn.Size = new System.Drawing.Size(96, 23);
            this.saveBtn.TabIndex = 10;
            this.saveBtn.Text = "Sla op";
            this.saveBtn.UseVisualStyleBackColor = true;
            this.saveBtn.Click += new System.EventHandler(this.saveBtn_Click);
            // 
            // scoreLocalBtn
            // 
            this.scoreLocalBtn.Location = new System.Drawing.Point(335, 608);
            this.scoreLocalBtn.Name = "scoreLocalBtn";
            this.scoreLocalBtn.Size = new System.Drawing.Size(151, 23);
            this.scoreLocalBtn.TabIndex = 11;
            this.scoreLocalBtn.Text = "Geef lokale score";
            this.scoreLocalBtn.UseVisualStyleBackColor = true;
            this.scoreLocalBtn.Click += new System.EventHandler(this.scoreLocalBtn_Click);
            // 
            // localScoreListBox
            // 
            this.localScoreListBox.FormattingEnabled = true;
            this.localScoreListBox.Location = new System.Drawing.Point(617, 536);
            this.localScoreListBox.Name = "localScoreListBox";
            this.localScoreListBox.Size = new System.Drawing.Size(372, 264);
            this.localScoreListBox.TabIndex = 12;
            // 
            // dataGridView3
            // 
            this.dataGridView3.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dataGridView3.Location = new System.Drawing.Point(567, 12);
            this.dataGridView3.Name = "dataGridView3";
            this.dataGridView3.Size = new System.Drawing.Size(539, 219);
            this.dataGridView3.TabIndex = 13;
            // 
            // AppForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1154, 833);
            this.Controls.Add(this.dataGridView3);
            this.Controls.Add(this.localScoreListBox);
            this.Controls.Add(this.scoreLocalBtn);
            this.Controls.Add(this.saveBtn);
            this.Controls.Add(this.hiLbl);
            this.Controls.Add(this.helloLbl);
            this.Controls.Add(this.giveScoreLbl);
            this.Controls.Add(this.selectGameLbl);
            this.Controls.Add(this.scoreTxtBox);
            this.Controls.Add(this.newGameTxtBox);
            this.Controls.Add(this.gamesComboBox);
            this.Controls.Add(this.dataGridView2);
            this.Controls.Add(this.dataGridView1);
            this.Name = "AppForm";
            this.Text = "Form1";
            this.Load += new System.EventHandler(this.AppForm_Load);
            ((System.ComponentModel.ISupportInitialize)(this.dataGridView1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridView2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridView3)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.DataGridView dataGridView1;
        private System.Windows.Forms.DataGridView dataGridView2;
        private System.Windows.Forms.ComboBox gamesComboBox;
        private System.Windows.Forms.TextBox newGameTxtBox;
        private System.Windows.Forms.TextBox scoreTxtBox;
        private System.Windows.Forms.Label selectGameLbl;
        private System.Windows.Forms.Label giveScoreLbl;
        private System.Windows.Forms.Label helloLbl;
        private System.Windows.Forms.Label hiLbl;
        private System.Windows.Forms.Button saveBtn;
        private System.Windows.Forms.Button scoreLocalBtn;
        private System.Windows.Forms.ListBox localScoreListBox;
        private System.Windows.Forms.DataGridView dataGridView3;
    }
}

