﻿using DataLayer;
using Globals.Interfaces;
using LogicLayer;
using System;
using System.Windows.Forms;

namespace MainForm
{
    static class Program
    {
        /// <summary>
        /// The main entry point for the application.
        /// </summary>
        [STAThread]
        static void Main()
        {
            IConnection mysql = new MySqlConnection();
            IConnection sqlite = new SQLiteConnectionCl();
            ILogic logic = new Logic(mysql, sqlite);
            Application.EnableVisualStyles();
            Application.SetCompatibleTextRenderingDefault(false);
            Application.Run(new AppForm(logic));
        }
    }
}
