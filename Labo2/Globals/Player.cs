﻿namespace Globals
{
    // Class which set a player name
    public class Player
    {
        // Property which contains the players name
        public string Name { get; set; }

        // Non-standard constructor of class Player
        public Player(string name)
        {
            this.Name = name;
        }
    }
}
