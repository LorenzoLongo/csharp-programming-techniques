﻿using System;
using System.Collections.Generic;
using System.Data;

namespace Globals.Interfaces
{
    // Contract for the Logic layer
    public interface ILogic
    {
        // DataSets for both of the databases (local and mysql)
        DataSet SQLiteDS { get; }
        DataSet MySQLDS { get; }

        // Method to get all gamenames inside the database
        List<Game> GetGamesAsList();
        // Method to insert new data inside the databases
        void InsertScore(string playerName, string gameName, int score, DateTime time);
        // Method to get the top 10 scores from players from both databases
        List<ScoreItem> GetTop10Scores(string gameName);
    }
}
