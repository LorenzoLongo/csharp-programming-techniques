﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Common;

namespace Globals.Interfaces
{
    // Contract for the connection classes
    public interface IConnection
    {
        // Property which contains the connection to a specific database
        DbConnection Connection { get; }
        // Property which contains a DataSet that saves all of the DataTables
        DataSet Set { get; }
        // List which contains Game-objects
        List<Game> Games { get; set; }

        // Method to create a connection with a specific database
        void CreateDBConnection(string providerName);

        // Method to update a specific database
        void Update(string playerName, string gameName, int score, DateTime time, bool containsPlayer, bool containsGame);

        // Method to get the top 10 of scores in a specific database
        List<ScoreItem> GetTop10Scores(string gameName);
    }
}
