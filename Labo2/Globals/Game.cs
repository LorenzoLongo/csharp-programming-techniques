﻿using System.Collections.Generic;


namespace Globals
{
    // Class which sets all of the games inside the database
    public class Game
    {
        // Property which contains the game name
        public string Name { get; set; }

        // Property which contains a List of game-objects
        public List<Game> Games { get; set; }

        // Standard constructor of class Game
        public Game()
        {

        }

        // Non-standard constructor of class Game
        public Game(string name)
        {
            this.Name = name;
        }
    }
}
