﻿namespace Globals
{
    // Struct of ScoreItems
    public struct ScoreItem
    {
        // Property which contains values of the Player class
        public Player Player { get; set; }

        // Property which contains scores 
        public int Score {get; set; }

        // Non-Standard constructor of ScoreItem struct
        public ScoreItem(Player player, int score)
        {
            this.Player = player;
            this.Score = score;
        }

        // ToString method override => concats scores with playernames
        public override string ToString()
        {
            return Score + " > " + Player.Name;
        }
    }
}
