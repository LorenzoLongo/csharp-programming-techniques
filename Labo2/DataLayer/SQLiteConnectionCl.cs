﻿using Globals;
using Globals.Interfaces;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.Common;
using System.Data.SQLite;
using System.Windows.Forms;

namespace DataLayer
{
    public class SQLiteConnectionCl : IConnection
    {
        // Private fields
        private bool checkIfTableExists = false;
        private int rowsPlayers, rowsGames;
        private DbCommand cmd;
        private DbDataAdapter dataAdapter;
        private DbProviderFactory factory;


        // Property which contains the Top 10 scores of a specific game inside the SQLite database
        private List<ScoreItem> Top10Scores { get; set; }
        // Property which contains the ConnectionString of the SQLite database
        private static string ConnectionString { get; set; }
        // Property which contains the Connection to the SQLite database
        public DbConnection Connection { get; private set; }
        // DataSet which contains all of the tables of the SQLite database
        public DataSet Set { get; private set; }
        // Property only for MySqlConnection class => Not implemented
        public List<Game> Games { get => throw new NotImplementedException(); set => throw new NotImplementedException(); }


        // Method to create a SQLite database connection
        public void CreateDBConnection(string providerName)
        {
            // Gets all the connections inside the App.config file (MainForm project)
            GetConnections(providerName);

            // Creates a DbProviderFactories, sets the connectionstring from the App.config file, creates a new DataAdapter
            if (ConnectionString != null)
            {
                try
                {
                    factory = DbProviderFactories.GetFactory(providerName);

                    Connection = factory.CreateConnection();
                    Connection.ConnectionString = ConnectionString;
                    this.dataAdapter = factory.CreateDataAdapter();
                }
                catch (Exception ex)
                {
                    if (Connection != null)
                    {
                        Connection = null;
                    }
                    Console.WriteLine(ex.Message);
                }
            }
            // Updates the DataSet 'Set'
            UpdateDataSet();
        }

        // Gets all the connections inside the App.Config file
        private static string GetConnections(string providerName)
        {
            ConnectionString = null;

            ConnectionStringSettingsCollection settings = ConfigurationManager.ConnectionStrings;

            // Searches the ConnectionString which matches the SQLite value and returns it
            if (settings != null)
            {
                foreach (ConnectionStringSettings cs in settings)
                {
                    if (cs.ProviderName.ToLower() == providerName.ToLower())
                    {
                        ConnectionString = cs.ConnectionString;
                        break;
                    }
                }
            }
            return ConnectionString;
        }

        // Method to update the DataSet 'Set'
        private void UpdateDataSet()
        {
            try
            {
                // Checks state of the connections and opens it, initializes new DataSet
                MessageBox.Show(Connection.State + "");
                Connection.Open();
                MessageBox.Show(Connection.State + "");
                Set = new DataSet();

                // Fills the Players table
                cmd = Connection.CreateCommand();
                cmd.CommandText = "select * from players;";
                cmd.CommandType = CommandType.Text;
                dataAdapter.SelectCommand = cmd;
                dataAdapter.Fill(Set, "Players");

                // Fils the Games table
                cmd.CommandText = "select * from games;";
                dataAdapter.Fill(Set, "Games");

                // Fills the Scores table
                cmd.CommandText = "select * from scores;";
                dataAdapter.Fill(Set, "Scores");
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message);
            }
            finally
            {
                Connection.Close();
            }
        }

        // Updates the SQLite database
        public void Update(string playerName, string gameName, int score, DateTime time, bool containsPlayer, bool containsGame)
        {
            string idPlayer = "", idGame = "";
            if (containsPlayer == true)
            {
                foreach (DataRow row in Set.Tables["Players"].Rows)
                {
                    if (row["name"].ToString() == playerName)
                    {
                        idPlayer = row["id"].ToString();
                    }
                }
            }
            else if (containsPlayer == false) InsertPlayer(playerName);

            if (containsGame == true)
            {
                foreach (DataRow row in Set.Tables["Games"].Rows)
                {
                    if (row["name"].ToString() == gameName)
                    {
                        idGame = row["id"].ToString();
                    }
                }
            }
            else if (containsGame == false) InsertGame(gameName);

            //if (idPlayer == "" && idGame == "") InsertScore(score);
            //else if (idPlayer == "" && idGame != "") InsertScore(idGame, score);
            //else if (idPlayer != "" && idGame == "") InsertScore(idPlayer, score, 1);
            //else if (idPlayer != "" && idGame != "") InsertScore(idPlayer, idGame, score, 1);
        }

        private void InsertPlayer(string playerName)
        {
            rowsPlayers = Set.Tables["Players"].Select().Length;
            ++rowsPlayers;
            using (SQLiteConnection conn = new SQLiteConnection(@"Data Source = C:\Users\Loren\source\repos\1718_CSProgTech_LongoLorenzo\Labo2\Globals\Sources\data.sqlite"))
            {
                SQLiteCommand cmd = new SQLiteCommand();
                cmd.CommandType = CommandType.Text;
                cmd.CommandText = "INSERT INTO players (id, name) VALUES (@id, @name);";
                cmd.Connection = conn;
                SQLiteParameter param = new SQLiteParameter();
                param.ParameterName = "@id";
                param.Value = rowsPlayers.ToString();

                SQLiteParameter param2 = new SQLiteParameter();
                param2.ParameterName = "@name";
                param2.Value = playerName;

                cmd.Parameters.Add(param);
                cmd.Parameters.Add(param2);

                conn.Open();
                cmd.ExecuteNonQuery();
                conn.Close();
            }
        }

        private void InsertGame(string gameName)
        {
            rowsGames = Set.Tables["Games"].Select().Length;
            ++rowsGames;
            using (SQLiteConnection conn = new SQLiteConnection(@"Data Source = C:\Users\Loren\source\repos\1718_CSProgTech_LongoLorenzo\Labo2\Globals\Sources\data.sqlite"))
            {
                SQLiteCommand cmd = new SQLiteCommand();
                cmd.CommandType = CommandType.Text;
                cmd.CommandText = "INSERT INTO games (id, name) VALUES (@id, @name);";
                cmd.Connection = conn;
                SQLiteParameter param = new SQLiteParameter();
                param.ParameterName = "@id";
                param.Value = rowsGames.ToString();

                SQLiteParameter param2 = new SQLiteParameter();
                param2.ParameterName = "@name";
                param2.Value = gameName;

                cmd.Parameters.Add(param);
                cmd.Parameters.Add(param2);

                conn.Open();
                cmd.ExecuteNonQuery();
                conn.Close();
            }
        }

        //private void InsertScore(int score)
        //{
        //    int rows = Set.Tables["Scores"].Select().Length;
        //    ++rows;
        //    using (SQLiteConnection conn = new SQLiteConnection(@"Data Source = C:\Users\Loren\source\repos\1718_CSProgTech_LongoLorenzo\Labo2\Globals\Sources\data.sqlite"))
        //    {
        //        SQLiteCommand cmd = new SQLiteCommand();
        //        cmd.CommandType = CommandType.Text;
        //        cmd.CommandText = "INSERT INTO scores (players_id, games_id, score, score_id) VALUES (@idPlayers, @idGames, @score, @scoreid);";
        //        cmd.Connection = conn;
        //        SQLiteParameter param = new SQLiteParameter();
        //        param.ParameterName = "@idPlayers";
        //        param.Value = rowsPlayers.ToString();

        //        SQLiteParameter param2 = new SQLiteParameter();
        //        param.ParameterName = "@idGames";
        //        param.Value = rowsGames.ToString();

        //        SQLiteParameter param3 = new SQLiteParameter();
        //        param2.ParameterName = "@score";
        //        param2.Value = score;

        //        SQLiteParameter param4 = new SQLiteParameter();
        //        param.ParameterName = "@scoreid";
        //        param.Value = rows.ToString();

        //        cmd.Parameters.Add(param);
        //        cmd.Parameters.Add(param2);
        //        cmd.Parameters.Add(param3);
        //        cmd.Parameters.Add(param4);

        //        conn.Open();
        //        cmd.ExecuteNonQuery();
        //        conn.Close();
        //    }
        //}

        //private void InsertScore(string idGame, int score)
        //{
        //    int rows = Set.Tables["Scores"].Select().Length;
        //    ++rows;

        //    using (SQLiteConnection conn = new SQLiteConnection(@"Data Source = C:\Users\Loren\source\repos\1718_CSProgTech_LongoLorenzo\Labo2\Globals\Sources\data.sqlite"))
        //    {
        //        SQLiteCommand cmd = new SQLiteCommand();
        //        cmd.CommandType = CommandType.Text;
        //        cmd.CommandText = "INSERT INTO scores (players_id, games_id, score, score_id) VALUES (@idPlayers, @idGames, @score, @scoreid);";
        //        SQLiteParameter param = new SQLiteParameter();
        //        param.ParameterName = "@idPlayers";
        //        param.Value = rowsPlayers.ToString();

        //        SQLiteParameter param2 = new SQLiteParameter();
        //        param.ParameterName = "@idGames";
        //        param.Value = idGame;

        //        SQLiteParameter param3 = new SQLiteParameter();
        //        param2.ParameterName = "@score";
        //        param2.Value = score;

        //        SQLiteParameter param4 = new SQLiteParameter();
        //        param.ParameterName = "@scoreid";
        //        param.Value = rows.ToString();

        //        cmd.Parameters.Add(param);
        //        cmd.Parameters.Add(param2);
        //        cmd.Parameters.Add(param3);
        //        cmd.Parameters.Add(param4);

        //        conn.Open();
        //        cmd.ExecuteNonQuery();
        //        conn.Close();
        //    }
        //}

        //private void InsertScore(string idPlayer, int score, int eP)
        //{
        //    int rows = Set.Tables["Scores"].Select().Length;
        //    ++rows;
        //    using (SQLiteConnection conn = new SQLiteConnection(@"Data Source = C:\Users\Loren\source\repos\1718_CSProgTech_LongoLorenzo\Labo2\Globals\Sources\data.sqlite"))
        //    {
        //        SQLiteCommand cmd = new SQLiteCommand();
        //        cmd.CommandType = CommandType.Text;
        //        cmd.CommandText = "INSERT INTO scores (players_id, games_id, score, score_id) VALUES (@idPlayers, @idGames, @score, @scoreid);";
        //        cmd.Connection = conn;
        //        SQLiteParameter param = new SQLiteParameter();
        //        param.ParameterName = "@idPlayers";
        //        param.Value = idPlayer;

        //        SQLiteParameter param2 = new SQLiteParameter();
        //        param.ParameterName = "@idGames";
        //        param.Value = rowsGames.ToString();

        //        SQLiteParameter param3 = new SQLiteParameter();
        //        param2.ParameterName = "@score";
        //        param2.Value = score;

        //        SQLiteParameter param4 = new SQLiteParameter();
        //        param.ParameterName = "@scoreid";
        //        param.Value = rows.ToString();

        //        cmd.Parameters.Add(param);
        //        cmd.Parameters.Add(param2);
        //        cmd.Parameters.Add(param3);
        //        cmd.Parameters.Add(param4);

        //        conn.Open();
        //        cmd.ExecuteNonQuery();
        //        conn.Close();
        //    }
        //}

        //private void InsertScore(string idPlayer, string idGame, int score, int eP)
        //{
        //    rowsPlayers = Set.Tables["Players"].Select().Length;
        //    ++rowsPlayers;
        //    rowsGames = Set.Tables["Games"].Select().Length;
        //    ++rowsGames;
        //    int rows = Set.Tables["Scores"].Select().Length;
        //    ++rows;
        //    using (SQLiteConnection conn = new SQLiteConnection(@"Data Source = C:\Users\Loren\source\repos\1718_CSProgTech_LongoLorenzo\Labo2\Globals\Sources\data.sqlite"))
        //    {
        //        SQLiteCommand cmd = new SQLiteCommand();
        //        cmd.CommandType = CommandType.Text;
        //        cmd.CommandText = "INSERT INTO scores (players_id, games_id, score, score_id) VALUES (@idPlayers, @idGames, @score, @scoreid);";
        //        cmd.Connection = conn;
        //        SQLiteParameter param = new SQLiteParameter();
        //        param.ParameterName = "@idPlayers";
        //        param.Value = idPlayer;

        //        SQLiteParameter param2 = new SQLiteParameter();
        //        param.ParameterName = "@idGames";
        //        param.Value = idGame;

        //        SQLiteParameter param3 = new SQLiteParameter();
        //        param2.ParameterName = "@score";
        //        param2.Value = score;

        //        SQLiteParameter param4 = new SQLiteParameter();
        //        param.ParameterName = "@scoreid";
        //        param.Value = rows.ToString();

        //        cmd.Parameters.Add(param);
        //        cmd.Parameters.Add(param2);
        //        cmd.Parameters.Add(param3);
        //        cmd.Parameters.Add(param4);

        //        conn.Open();
        //        cmd.ExecuteNonQuery();
        //        conn.Close();
        //    }
        //}

        // Method to provide the top 10 scores of the SQLite database from a particular selected game
        public List<ScoreItem> GetTop10Scores(string gameName)
        {
            FillTop10Values(gameName);

            // Initialize new ScoreItem List, Add each row to the ScoreItem List and return the List
            Top10Scores = new List<ScoreItem>();
            foreach (DataRow item in Set.Tables["TOP10SQLite"].Rows)
            {
                int score = Int32.Parse(item["score"].ToString());
                Top10Scores.Add(new ScoreItem(new Player((string)item["name"]), score));
            }

            checkIfTableExists = true;
            return Top10Scores;
        }

        // Method to Fill the DataSet Set with the top 10 score of players for a certain game
        private void FillTop10Values(string gameName)
        {
            using (SQLiteConnection conn = new SQLiteConnection(@"Data Source = C:\Users\Loren\source\repos\1718_CSProgTech_LongoLorenzo\Labo2\Globals\Sources\data.sqlite"))
            {
                // Checks if the DataTable exists, if yes use clear method
                if (checkIfTableExists == true) Set.Tables["Top10SQLite"].Clear();

                // Initializes new DataAdapter and Command, sets Command and Connection
                SQLiteDataAdapter dataAdaptTop10 = new SQLiteDataAdapter();
                SQLiteCommand cmdTop10 = new SQLiteCommand();
                cmdTop10.CommandType = CommandType.Text;
                cmdTop10.CommandText = "select players.name, scores.score from ((games inner join scores on games.id = scores.games_id) inner join players on scores.players_id = players.id) where games.name = @Game order by scores.score desc limit 10;";
                cmdTop10.Connection = conn;

                // Adds a new Parameter to the Command
                SQLiteParameter gameParam = new SQLiteParameter();
                gameParam.ParameterName = "@Game";
                gameParam.Value = gameName;
                cmdTop10.Parameters.Add(gameParam);

                // Connect the DataAdapter to the Command, Open Connection, Fill DataTable, Close Connection
                dataAdaptTop10.SelectCommand = cmdTop10;
                conn.Open();
                dataAdaptTop10.Fill(Set, "TOP10SQLite");
                conn.Close();
            }
        }
    }
}
