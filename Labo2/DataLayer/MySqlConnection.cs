﻿using Globals;
using Globals.Interfaces;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.Common;
using System.Linq;
using System.Windows.Forms;

namespace DataLayer
{
    public class MySqlConnection : IConnection
    {
        private DbCommand cmd;
        private DbDataAdapter dataAdapter;
        private DbProviderFactory factory;

        private static string ConnectionString { get; set; }
        public DbConnection Connection { get; private set; }

        public List<Game> Games { get; set; }

        public DataSet Set { get; set; }

        private static string GetConnections(string providerName)
        {
            ConnectionString = null;

            ConnectionStringSettingsCollection settings = ConfigurationManager.ConnectionStrings;


            if (settings != null)
            {
                foreach (ConnectionStringSettings cs in settings)
                {
                    if (cs.ProviderName.ToLower() == providerName.ToLower())
                    {
                        ConnectionString = cs.ConnectionString;
                        break;
                    }
                }
            }
            return ConnectionString;
        }

        public void CreateDBConnection(string providerName)
        {
            GetConnections(providerName);

            if (ConnectionString != null)
            {
                try
                {
                    factory = DbProviderFactories.GetFactory(providerName);

                    Connection = factory.CreateConnection();
                    Connection.ConnectionString = ConnectionString;
                    this.dataAdapter = factory.CreateDataAdapter();
                }
                catch (Exception ex)
                {
                    if (Connection != null)
                    {
                        Connection = null;
                    }
                    Console.WriteLine(ex.Message);
                }
            }
            ConnectionTest(providerName);
        }

        public List<ScoreItem> GetTop10Scores(string gameName)
        {
            throw new NotImplementedException();
        }

        public void Update(string playerName, string gameName, int score, DateTime time, bool containsPlayer, bool containsGame)
        {
            throw new NotImplementedException();
        }

        public void ConnectionTest(string provider)
        {
            try
            {
                MessageBox.Show(Connection.State + "");
                Connection.Open();
                MessageBox.Show(Connection.State + "");

            }
            catch (Exception ex)
            {

                Console.WriteLine(ex.Message);
            }

            if (provider == "MySql.Data.MySqlClient")
            {
                cmd = Connection.CreateCommand();
                cmd.CommandText = "select name from games;";
                cmd.CommandType = CommandType.Text;
                dataAdapter.SelectCommand = cmd;
                Set = new DataSet();
                dataAdapter.Fill(Set, "Games");
                FillGameList();
                UpdateDataSet();
            }
        }

        public void UpdateDataSet()
        {
            try
            {
                // Checks state of the connections and opens it, initializes new DataSet
                MessageBox.Show(Connection.State + "");
                Connection.Open();
                MessageBox.Show(Connection.State + "");
                Set = new DataSet();

                // Fills the Players table
                cmd = Connection.CreateCommand();
                cmd.CommandText = "select * from players;";
                cmd.CommandType = CommandType.Text;
                dataAdapter.SelectCommand = cmd;
                dataAdapter.Fill(Set, "Players");

                // Fils the Games table
                cmd.CommandText = "select * from games;";
                dataAdapter.Fill(Set, "Games");

                // Fills the Scores table
                cmd.CommandText = "select * from scores;";
                dataAdapter.Fill(Set, "Scores");
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message);
            }
            finally
            {
                Connection.Close();
            }
        }

        private void FillGameList()
        {
            Games = new List<Game>();
            Games = Set.Tables[0].AsEnumerable().Select(dataRow => new Game { Name = dataRow.Field<string>("name") }).ToList();
        }
    }
}
