﻿using Globals;
using Globals.Interfaces;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;

namespace LogicLayer
{
    public class Logic : ILogic
    {
        public DataSet SQLiteDS { get; private set; }
        public DataSet MySQLDS { get; private set; }
        public List<ScoreItem> GetTopScores { get; private set; }


        private readonly string sQLiteConn = "System.Data.SQLite";
        private readonly string mySqlConn = "MySql.Data.MySqlClient";
        private readonly IConnection mysql;
        private readonly IConnection sqlite;

        public Logic(IConnection mysql, IConnection sqlite)
        {
            this.mysql = mysql;
            this.sqlite = sqlite;
            sqlite.CreateDBConnection(sQLiteConn);
            mysql.CreateDBConnection(mySqlConn);

            SQLiteDS = sqlite.Set;
            MySQLDS = mysql.Set;
        }

        public void Update()
        {
            sqlite.CreateDBConnection(sQLiteConn);
            SQLiteDS = sqlite.Set;

            mysql.CreateDBConnection(mySqlConn);
            MySQLDS = mysql.Set;
        }

        public void InsertScore(string playerName, string gameName, int score, DateTime time)
        {
            bool containsPlayer = SQLiteDS.Tables["Players"].AsEnumerable().Any(row => playerName.ToLower() == row.Field<String>("name").ToLower());
            bool containsGame = SQLiteDS.Tables["Games"].AsEnumerable().Any(row => gameName.ToLower() == row.Field<String>("name").ToLower());

            //bool containsPlayerMySQL = MySQLDS.Tables["Players"].AsEnumerable().Any(row => playerName.ToLower() == row.Field<String>("name").ToLower());
            //bool containsGameMySQL = MySQLDS.Tables["Games"].AsEnumerable().Any(row => gameName.ToLower() == row.Field<String>("name").ToLower());

            int rows = SQLiteDS.Tables["Players"].Select().Length;
            ++rows;

            sqlite.Update(playerName, gameName, score, time, containsPlayer, containsGame);
            //mysql.Update(playerName, gameName, score, time, containsPlayerMySQL, containsGameMySQL);
            Update();
        }

        public List<ScoreItem> GetTop10Scores(string gameName)
        {
            GetTopScores = sqlite.GetTop10Scores(gameName);
            return GetTopScores;
        }

        public List<Game> GetGamesAsList()
        {
            return mysql.Games;
        }
    }
}
