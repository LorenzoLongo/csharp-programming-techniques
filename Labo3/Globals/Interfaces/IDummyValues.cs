﻿namespace Globals.Interfaces
{
    // Interface which contains all of the methods to add data to the created database
    public interface IDummyValues
    {
        void DummyProperties();
        void DummyCharacteristics();
        void DummyBuyersRenters();
        void DummyAgreements();
        void ConnectBuildingsChars();
    }
}
