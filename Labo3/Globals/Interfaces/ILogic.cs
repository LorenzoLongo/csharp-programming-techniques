﻿using System;
using System.Collections.Generic;

namespace Globals.Interfaces
{
    // Interface which contains all of the Logical Layer methods 
    public interface ILogic
    {
        // Methods ManageCustomersForm
        List<string> GetCustomers();
        void AddCustomer(string surname, string name);
        void DeleteCustomer(int id);
        int CheckIfIDExists();


        // Methods ManagePropertiesForm
        List<string> GetGroundTypes();
        List<string> GetBuildingTypes();

        void AddBuilding(string town, string street, object type, int houseNr);
        void AddBuilding(string town, string street, object type, int boxOrRooms, int houseNr, bool roomsOrBoxNr);
        void AddBuilding(string town, string street, object type, int houseNr, double epc);
        void AddBuilding(string town, string street, object type, int boxOrRooms, int houseNr, double epc, bool roomsOrBoxNr);
        void AddBuilding(string town, string street, object type, int rooms, int houseNr, int boxNr);
        void AddBuilding(string town, string street, object type, int rooms, int houseNr, int boxNr, double epc);

        void AddGround(string town, string street, object type);
        void AddGround(string town, string street, double surface, object type);


        // Methods ManageCharacteristicsForm
        List<string> GetCharacteristics();
        List<string> GetProperties();
        List<int> GetCheckedChars(int selectedId);

        void UpdateCheckedChars(int selectedCharIndex, int propertyID, bool checkedUnchecked);


        // Methods ManageAgreementsForm
        List<string> GetNoAgreements();

        void AddSellAgreement(DateTime date, int property, int buyerRenter, double price);

        void AddRentAgreement(DateTime date, int property, int buyerRenter, double guarantee, double monthlyPayment);
        void AddRentAgreement(DateTime date, int property, int buyerRenter, double baseCosts, double guarantee, double monthlyPayment);


        // Methods StopAgreementsForm
        List<string> GetAgreements();
        void TerminateAgreement(int propertyID);
        bool GroundOrBuilding(int propertyID);


        // Method to save changes to the database
        void Commit();
    }
}
