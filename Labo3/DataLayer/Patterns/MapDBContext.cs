﻿using DataLayer.Code_First_Classes;
using MySql.Data.Entity;
using System.Data.Entity;

namespace DataLayer.Patterns
{
    // Class which implements the DbContext class
    [DbConfigurationType(typeof(MySqlEFConfiguration))]
    public class MapDBContext : DbContext
    {
        // DbSets for each table
        public virtual DbSet<Agreements> Agreements { get; set; }
        public virtual DbSet<BuyersRenters> BuyersRenters { get; set; }
        public virtual DbSet<Properties> Properties { get; set; }
        public virtual DbSet<Characteristics> Characteristics { get; set; }
        public virtual DbSet<Buildings> Buildings { get; set; }

        // Constructor of MapDBContext
        public MapDBContext(string connectionString) : base(connectionString)
        {

        }
    }
}
