﻿using DataLayer.Interfaces;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;

namespace DataLayer.Patterns
{
    // Class which implements the Repository Pattern interface
    public class GenericRepository<T> : IRepository<T> where T : EntityBase
    {
        private readonly MapDBContext _dbContext;

        private IDbSet<T> _dbSet => _dbContext.Set<T>();

        // Non-standard constructor
        public GenericRepository(MapDBContext dbContext)
        {
            _dbContext = dbContext;
        }

        // Method to add values to a table
        public void Add(T entity)
        {
            _dbSet.Add(entity);
        }

        // Method to delete values in a table
        public void Delete(T entity)
        {
            _dbSet.Remove(entity);
        }

        // Method to return a value inside a table from a selected ID
        public T GetById(int id)
        {
            return _dbSet.Find(id);
        }

        // Method to return a List of values inside a table
        public IEnumerable<T> List()
        {
            return _dbSet.AsEnumerable();
        }

        // Method to update values inside a table
        public void Update(T entity)
        {
            _dbSet.Attach(entity);
            _dbContext.Entry(entity).State = EntityState.Modified;
        }
    }
}
