﻿using DataLayer.Code_First_Classes;
using DataLayer.Interfaces;

namespace DataLayer.Patterns
{
    // Class which implements the IUnitOfWork interface
    public class UnitOfWork : IUnitOfWork
    {
        // MapDBContext initializer
        private readonly MapDBContext _dbContext;

        // Fields to interact with the Repository Pattern class methods
        public IRepository<BuyersRenters> BuyersRentersRepo => new GenericRepository<BuyersRenters>(_dbContext);
        public IRepository<Agreements> AgreementsRepo => new GenericRepository<Agreements>(_dbContext);
        public IRepository<Properties> PropertiesRepo => new GenericRepository<Properties>(_dbContext);
        public IRepository<Grounds> GroundsRepo => new GenericRepository<Grounds>(_dbContext);
        public IRepository<Buildings> BuildingsRepo => new GenericRepository<Buildings>(_dbContext);
        public IRepository<RentAgreements> RentAgreementsRepo => new GenericRepository<RentAgreements>(_dbContext);
        public IRepository<BuyAgreements> BuyAgreementsRepo => new GenericRepository<BuyAgreements>(_dbContext);
        public IRepository<Characteristics> CharacteristicsRepo => new GenericRepository<Characteristics>(_dbContext);

        // Non-standard constructor
        public UnitOfWork(MapDBContext dbContext)
        {
            _dbContext = dbContext;
        }

        // Method to save the changes to the database
        public void Commit()
        {
            _dbContext.SaveChanges();
        }
    }
}
