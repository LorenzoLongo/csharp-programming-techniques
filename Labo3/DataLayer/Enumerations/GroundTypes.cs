﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DataLayer.Enumerations
{
    [Flags]
    public enum GroundTypes
    {
        Weiland = 1,
        Bouwgrond = 2,
        Landbouwgrond = 3
    }
}
