﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DataLayer.Enumerations
{
    [Flags]
    public enum BuildingTypes
    {
        [Display(Name = "Appartement")]
        Appartement = 1,
        [Display(Name = "Gesloten Bebouwing")]
        GeslotenBebouwing = 2,
        [Display(Name = "Open Bebouwing")]
        OpenBebouwing = 3,
        [Display(Name = "Half-open Bebouwing")]
        HalfOpenBebouwing = 4
    }
}
