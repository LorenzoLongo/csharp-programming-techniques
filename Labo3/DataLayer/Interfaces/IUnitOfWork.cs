﻿using DataLayer.Code_First_Classes;

namespace DataLayer.Interfaces
{
    // Interface of Unit of Work
    public interface IUnitOfWork
    {
        IRepository<BuyersRenters> BuyersRentersRepo { get; }
        IRepository<Agreements> AgreementsRepo { get; }
        IRepository<Properties> PropertiesRepo { get; }
        IRepository<Grounds> GroundsRepo { get; }
        IRepository<Buildings> BuildingsRepo { get; }
        IRepository<Characteristics> CharacteristicsRepo { get; }

        void Commit();
    }
}
