﻿using DataLayer.Patterns;
using System.Collections.Generic;

namespace DataLayer.Interfaces
{
    // Interface of Repository Pattern
    public interface IRepository<T> where T : EntityBase
    {
        T GetById(int id);
        IEnumerable<T> List();
        void Add(T entity);
        void Delete(T entity);
        void Update(T entity);
    }
}
