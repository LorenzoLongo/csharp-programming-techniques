﻿using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace DataLayer.Code_First_Classes
{
    // Class to create the BuyAgreements table (inherit from Agreements class)
    [Table("verkoopsovereenkomsten")]
    public partial class BuyAgreements : Agreements
    {
        [Column("verkoopprijs"), Required]
        public double SellingPrice { get; set; }
    }
}
