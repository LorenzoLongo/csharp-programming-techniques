﻿using DataLayer.Patterns;
using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace DataLayer.Code_First_Classes
{
    // Class to create the Agreements table
    [Table("overeenkomsten")]
    public abstract class Agreements : EntityBase
    {
        [Key, Column("id")]
        public override int Id { get; set; }
        [Column("datum"), Required]
        public DateTime Date { get; set; }

        [Column("kopers_huurders_id"), Required]
        public virtual BuyersRenters BuyersRenters { get; set; }
        [Column("eigendom_id"), Required]
        public virtual Properties Properties { get; set; }
    }
}
