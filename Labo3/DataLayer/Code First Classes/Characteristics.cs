﻿using DataLayer.Patterns;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace DataLayer.Code_First_Classes
{
    // Class to create the Characteristics table
    [Table("eigenschappen")]
    public class Characteristics : EntityBase
    {
        [Key, Column("id")]
        public override int Id { get; set; }
        [Column("type")]
        public string Type { get; set; }

        public virtual ICollection<Buildings> Buildings { get; set; }
    }
}
