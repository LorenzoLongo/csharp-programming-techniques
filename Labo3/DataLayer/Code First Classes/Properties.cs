﻿using DataLayer.Patterns;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace DataLayer.Code_First_Classes
{
    // Class to create the Properties table
    [Table("eigendommen")]
    public abstract class Properties : EntityBase
    {
        [Key, Column("id")]
        public override int Id { get; set; }
        [Column("gemeente"), Required]
        public string Town { get; set; }
        [Column("straat"), Required]
        public string Street { get; set; }

        public virtual List<Agreements> Agreements { get; set; }
    }
}
