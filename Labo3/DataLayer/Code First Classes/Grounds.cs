﻿using DataLayer.Enumerations;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace DataLayer.Code_First_Classes
{
    // Class to create the Grounds table (inherit from Properties class)
    [Table("gronden")]
    public partial class Grounds : Properties
    {
        [Column("opp")]
        public double? Surface { get; set; }
        [EnumDataType(typeof(GroundTypes)), Column("type"), Required]
        public GroundTypes Types { get; set; }
    }
}
