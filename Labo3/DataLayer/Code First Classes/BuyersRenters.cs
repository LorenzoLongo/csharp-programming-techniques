﻿using DataLayer.Patterns;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace DataLayer.Code_First_Classes
{
    // Class to create the BuyersRenters table
    [Table("kopers_huurders")]
    public class BuyersRenters : EntityBase
    {
        [Key, Column("id")]
        public override int Id { get; set; }
        [Column("naam")]
        public string Name { get; set; }
        [Column("voornaam")]
        public string Surname { get; set; }

        public virtual List<Agreements> Agreements { get; set; }
    }
}
