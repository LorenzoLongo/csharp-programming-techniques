﻿using DataLayer.Enumerations;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace DataLayer.Code_First_Classes
{
    // Class to create the Buildings table (inherit from Properties class)
    [Table("panden")]
    public partial class Buildings : Properties
    {
        [Column("type"), EnumDataType(typeof(BuildingTypes))]
        public BuildingTypes? Types { get; set; }
        [Column("kamers")]
        public int? Rooms { get; set; }
        [Column("huisnr"), Required]
        public int HouseNr { get; set; }
        [Column("busnr")]
        public int? BoxNr { get; set; }
        [Column("EPC")]
        public double? EPC { get; set; }

        public virtual ICollection<Characteristics> Characteristics { get; set; }
    }
}
