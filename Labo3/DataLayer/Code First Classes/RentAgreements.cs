﻿using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace DataLayer.Code_First_Classes
{
    // Class to create the RentAgreements table (inherit from Agreements class)
    [Table("verhuurovereenkomsten")]
    public partial class RentAgreements : Agreements
    {
        [Column("vaste kosten")]
        public double? FixedCosts { get; set; }
        [Column("waarborg"), Required]
        public double Guarantee { get; set; }
        [Column("maandbedrag"), Required]
        public double MonthlyCost { get; set; }
    }
}
