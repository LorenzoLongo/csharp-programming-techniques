﻿using DataLayer.Code_First_Classes;
using DataLayer.Enumerations;
using DataLayer.Interfaces;
using DataLayer.Patterns;
using Globals.Interfaces;
using System;
using System.Collections.Generic;
using System.Linq;

namespace LogicLayer
{
    // Class which contains all of the method to interact with the database and return it to the forms
    public class Logic : ILogic
    {
        // Initializers
        private IUnitOfWork unit;
        private MapDBContext dBContext;

        // Non-standard constructor
        public Logic(IUnitOfWork unit, MapDBContext dbContext)
        {
            this.unit = unit;
            this.dBContext = dbContext;
        }


        // ManageCustomersForm methods 

        // Method to get all of the buyers and renters inside of the buyers_renters (kopers_huurders) table
        // Returns list of all the customers
        public List<string> GetCustomers()
        {
            List<string> customers = new List<string>();
            foreach (BuyersRenters br in dBContext.BuyersRenters.Where(br => br.Id != null).ToList())
            {
                customers.Add(br.Id + " " + br.Surname + " " + br.Name);
            }
            return customers;
        }

        // Method which adds the in-form inserted values into the buyers_renters (kopers_huurders) table
        public void AddCustomer(string surname, string name)
        {
            unit.BuyersRentersRepo.Add(new BuyersRenters() { Name = name, Surname = surname });
            unit.Commit();
        }

        // Method which deletes the in-form inserted ID from the buyers_renters (kopers_huurders) table
        public void DeleteCustomer(int id)
        {
            unit.BuyersRentersRepo.Delete(unit.BuyersRentersRepo.GetById(id));
            unit.Commit();
        }

        // Method which checks if the inserted ID to delete a certain costumer exists
        public int CheckIfIDExists()
        {
            var getID = dBContext.BuyersRenters.OrderByDescending(id => id.Id).FirstOrDefault<BuyersRenters>();
            var checkID = getID.Id;

            return checkID;
        }


        // ManagePropertiesForm methods

        // Method which gets and returns all of the ground types inside the GroundTypes enum
        public List<string> GetGroundTypes()
        {
            List<string> types = new List<string>();
            foreach (GroundTypes ground in Enum.GetValues(typeof(GroundTypes)))
            {
                types.Add(ground.ToString());
            }
            return types;
        }

        // Method which gets and returns all of the building types inside the BuildingsTypes enum
        public List<string> GetBuildingTypes()
        {
            List<string> types = new List<string>();
            foreach (BuildingTypes building in Enum.GetValues(typeof(BuildingTypes)))
            {
                types.Add(building.ToString());
            }
            return types;
        }

        // Method to insert values in the building table
        public void AddBuilding(string town, string street, object type, int houseNr)
        {
            var types = type.ToString();
            unit.PropertiesRepo.Add(new Buildings() { Town = town, Street = street, Types = (BuildingTypes)Enum.Parse(typeof(BuildingTypes), types), HouseNr = houseNr });
            unit.Commit();
        }

        // Method to insert values in the building table
        public void AddBuilding(string town, string street, object type, int boxOrRooms, int houseNr, bool roomsOrBoxNr)
        {
            var types = type.ToString();
            if (roomsOrBoxNr == true)
                unit.PropertiesRepo.Add(new Buildings() { Town = town, Street = street, Types = (BuildingTypes)Enum.Parse(typeof(BuildingTypes), types), Rooms = boxOrRooms, HouseNr = houseNr });
            else
                unit.PropertiesRepo.Add(new Buildings() { Town = town, Street = street, Types = (BuildingTypes)Enum.Parse(typeof(BuildingTypes), types), HouseNr = houseNr, BoxNr = boxOrRooms });
            unit.Commit();
        }

        // Method to insert values in the building table
        public void AddBuilding(string town, string street, object type, int houseNr, double epc)
        {
            var types = type.ToString();
            unit.PropertiesRepo.Add(new Buildings() { Town = town, Street = street, Types = (BuildingTypes)Enum.Parse(typeof(BuildingTypes), types), HouseNr = houseNr, EPC = epc });
            unit.Commit();
        }

        // Method to insert values in the building table
        public void AddBuilding(string town, string street, object type, int boxOrRooms, int houseNr, double epc, bool roomsOrBoxNr)
        {
            var types = type.ToString();
            if (roomsOrBoxNr == true)
                unit.PropertiesRepo.Add(new Buildings() { Town = town, Street = street, Types = (BuildingTypes)Enum.Parse(typeof(BuildingTypes), types), Rooms = boxOrRooms, HouseNr = houseNr, EPC = epc });
            else
                unit.PropertiesRepo.Add(new Buildings() { Town = town, Street = street, Types = (BuildingTypes)Enum.Parse(typeof(BuildingTypes), types), HouseNr = houseNr, BoxNr = boxOrRooms, EPC = epc });
            unit.Commit();
        }

        // Method to insert values in the building table
        public void AddBuilding(string town, string street, object type, int rooms, int houseNr, int boxNr)
        {
            var types = type.ToString();
            unit.PropertiesRepo.Add(new Buildings() { Town = town, Street = street, Types = (BuildingTypes)Enum.Parse(typeof(BuildingTypes), types), Rooms = rooms, HouseNr = houseNr, BoxNr = boxNr });
            unit.Commit();
        }

        // Method to insert values in the building table
        public void AddBuilding(string town, string street, object type, int rooms, int houseNr, int boxNr, double epc)
        {
            var types = type.ToString();
            unit.PropertiesRepo.Add(new Buildings() { Town = town, Street = street, Types = (BuildingTypes)Enum.Parse(typeof(BuildingTypes), types), Rooms = rooms, HouseNr = houseNr, BoxNr = boxNr, EPC = epc });
            unit.Commit();
        }

        // Method to insert values into the grounds table
        public void AddGround(string town, string street, object type)
        {
            var types = type.ToString();
            unit.PropertiesRepo.Add(new Grounds() { Town = town, Street = street, Types = (GroundTypes)Enum.Parse(typeof(GroundTypes), types) });
            unit.Commit();
        }

        // Method to insert values into the grounds table
        public void AddGround(string town, string street, double surface, object type)
        {
            var types = type.ToString();
            unit.PropertiesRepo.Add(new Grounds() { Town = town, Street = street, Surface = surface, Types = (GroundTypes)Enum.Parse(typeof(GroundTypes), types) });
            unit.Commit();
        }


        // ManageCharacteristicsForm methods

        // Gets and returns all of the characteristics inside the Characteristics (eigenschappen) table
        public List<string> GetCharacteristics()
        {
            List<string> characteristics = new List<string>();
            foreach (Characteristics chars in dBContext.Characteristics.Where(chars => chars.Id != null).ToList())
            {
                characteristics.Add(chars.Type);
            }
            return characteristics;
        }

        // Gets and returns all of the properties inside the Properties (eigendommen) table
        public List<string> GetProperties()
        {
            List<string> properties = new List<string>();
            foreach (Buildings building in dBContext.Buildings.Where(build => build.Id != null).ToList())
            {
                properties.Add(building.Id + " " + building.Street + " " + building.Town + " " + building.HouseNr);
            }
            return properties;
        }

        // Gets and sets all the Charteristics that are connected with the specific building
        public List<int> GetCheckedChars(int selectedId)
        {
            var list = dBContext.Buildings.Where(id => id.Id == selectedId).Select(d => d.Characteristics).ToList();

            List<int> checkedChar = new List<int>();
            foreach (ICollection<Characteristics> v in list)
            {
                foreach (var c in v.Where(g => g.Id != null).Select(id => id.Id).ToList())
                {
                    int vs = c;
                    checkedChar.Add(vs);
                }
            }
            return checkedChar;
        }

        // Method to update the checked or unchecked Characteristics for a certain building
        public void UpdateCheckedChars(int selectedCharIndex, int propertyID, bool checkedUnchecked)
        {
            var newchar = dBContext.Characteristics.Where(s => s.Id == (selectedCharIndex + 1)).FirstOrDefault<Characteristics>();
            var newProp = dBContext.Buildings.Where(s => s.Id == propertyID).FirstOrDefault<Buildings>();

            // Check if checked or unchecked 
            if (checkedUnchecked == true)
            {
                unit.BuildingsRepo.Update(newProp);
                newchar.Buildings.Add(newProp);
            }
            else if (checkedUnchecked == false)
            {
                unit.BuildingsRepo.Update(newProp);
                newchar.Buildings.Remove(newProp);
            }
            unit.Commit();
        }


        // ManageAgreementsForm methods

        // Gets all the properties that don't have an agreement yet
        public List<string> GetNoAgreements()
        {
            var propIDs = dBContext.Properties.Where(id => id.Id != null).ToList();
            var agreementIDs = dBContext.Agreements.Where(id => id.Properties.Id != null).ToList();

            List<Properties> allProperties = new List<Properties>();
            List<Properties> matchingProperties = new List<Properties>();

            // Fill all properties list and fill properties list with properties that match with the ID inside the Agreements table
            foreach (var propID in propIDs)
            {
                allProperties.Add(propID);
                foreach (var agreeID in agreementIDs)
                {
                    if (propID.Id.ToString() == agreeID.Properties.Id.ToString())
                    {
                        matchingProperties.Add(propID);
                    }
                }
            }

            // Remove all of the matching values out of the allProperties list
            allProperties.RemoveAll(x => matchingProperties.Exists(y => y.Id == x.Id && y.Street == y.Street && x.Town == x.Town));
            List<string> noAgreement = new List<string>();

            foreach (Properties pr in allProperties)
            {
                noAgreement.Add(pr.Id.ToString() + " " + pr.Street.ToString() + " " + pr.Town.ToString());
            }

            return noAgreement;
        }

        // Method which adds a new Sell agreement to the Agreements table
        public void AddSellAgreement(DateTime date, int property, int buyerRenter, double price)
        {
            var prop = dBContext.Properties.Where(pr => pr.Id == property).ToList();
            var buyRenter = dBContext.BuyersRenters.Where(br => br.Id == buyerRenter).ToList();

            unit.AgreementsRepo.Add(new BuyAgreements() { Date = date, Properties = prop.FirstOrDefault(), BuyersRenters = buyRenter.FirstOrDefault(), SellingPrice = price });
        }

        // Method which adds a new Rent agreement to the Agreements table
        public void AddRentAgreement(DateTime date, int property, int buyerRenter, double guarantee, double monthlyPayment)
        {
            var prop = dBContext.Properties.Where(pr => pr.Id == property).ToList();
            var buyRenter = dBContext.BuyersRenters.Where(br => br.Id == buyerRenter).ToList();

            unit.AgreementsRepo.Add(new RentAgreements() { Date = date, Properties = prop.FirstOrDefault(), BuyersRenters = buyRenter.FirstOrDefault(), Guarantee = guarantee, MonthlyCost = monthlyPayment });
            unit.Commit();
        }

        // Method which adds a new Rent agreement to the Agreements table
        public void AddRentAgreement(DateTime date, int property, int buyerRenter, double baseCosts, double guarantee, double monthlyPayment)
        {
            var prop = dBContext.Properties.Where(pr => pr.Id == property).ToList();
            var buyRenter = dBContext.BuyersRenters.Where(br => br.Id == buyerRenter).ToList();

            unit.AgreementsRepo.Add(new RentAgreements() { Date = date, Properties = prop.FirstOrDefault(), BuyersRenters = buyRenter.FirstOrDefault(), FixedCosts = baseCosts, Guarantee = guarantee, MonthlyCost = monthlyPayment });
            unit.Commit();
        }


        // StopAgreementsForm methods

        // Method to get all of the properties that have an agreement
        public List<string> GetAgreements()
        {
            var propIDs = dBContext.Properties.Where(id => id.Id != null).ToList();
            var agreementIDs = dBContext.Agreements.Where(id => id.Properties.Id != null).ToList();

            List<string> properties = new List<string>();

            foreach (var propID in propIDs)
            {
                foreach (var agreeID in agreementIDs)
                {
                    if (propID.Id.ToString() == agreeID.Properties.Id.ToString())
                    {
                        properties.Add(propID.Id + " " + propID.Town + " " + propID.Street);
                    }
                }
            }
            return properties;
        }

        // Method to terminate an agreement of a property
        public void TerminateAgreement(int propertyID)
        {
            int id = 0;
            var agreement = dBContext.Agreements.Where(agree => agree.Properties.Id == propertyID).ToList();

            foreach (Agreements ag in agreement)
            {
                id = ag.Id;
            }

            unit.AgreementsRepo.Delete(unit.AgreementsRepo.GetById(id));
            unit.Commit();
        }

        // Method to decide if a property is a ground of a building
        public bool GroundOrBuilding(int propertyID)
        {
            var buildings = dBContext.Buildings.Where(b => b.Id != null).ToList();

            foreach (Buildings building in buildings)
            {
                if (propertyID == building.Id) return true;
            }

            return false;
        }

        // Method to save changes to the database
        public void Commit()
        {
            unit.Commit();
        }
    }



}
