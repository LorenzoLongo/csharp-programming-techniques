﻿using DataLayer.Code_First_Classes;
using DataLayer.Enumerations;
using DataLayer.Interfaces;
using DataLayer.Patterns;
using Globals.Interfaces;
using System;
using System.Linq;

namespace LogicLayer
{
    // Class which contains all of the methods to add values inside of the code first created database
    public class DummyValues : IDummyValues
    {
        private IUnitOfWork unitOfWork;
        private MapDBContext dbContext;

        // Non-standard constructor
        public DummyValues(IUnitOfWork unit, MapDBContext dbContext)
        {
            this.unitOfWork = unit;
            this.dbContext = dbContext;
        }

        // Method to add values to the Properties (eigendommen) table
        public void DummyProperties()
        {
            var newProperty = new Grounds() { Town = "Ninove", Street = "Ninovelaan", Types = GroundTypes.Bouwgrond };
            unitOfWork.PropertiesRepo.Add(newProperty);

            var newProperty2 = new Buildings() { Town = "Aalter", Street = "Fictiestraat", Rooms = 4, HouseNr = 215 };
            unitOfWork.PropertiesRepo.Add(newProperty2);

            var newProperty3 = new Buildings() { Town = "Aalst", Street = "Dummystraat", Types = BuildingTypes.Appartement, Rooms = 2, HouseNr = 33, BoxNr = 3 };
            unitOfWork.PropertiesRepo.Add(newProperty3);

            var newProperty4 = new Grounds() { Town = "Sint-Denijs", Street = "Kortrijkseteenweg", Surface = 3000.79, Types = GroundTypes.Bouwgrond };
            unitOfWork.PropertiesRepo.Add(newProperty4);

            var newProperty5 = new Grounds() { Town = "Wondelgem", Street = "Wondelgemstraat", Surface = 257.33, Types = GroundTypes.Weiland };
            unitOfWork.PropertiesRepo.Add(newProperty5);

            var newProperty6 = new Buildings() { Town = "Ledeberg", Street = "Lederberglaan", Types = BuildingTypes.HalfOpenBebouwing, HouseNr = 79, EPC = 57.23 };
            unitOfWork.PropertiesRepo.Add(newProperty6);
            unitOfWork.Commit();
        }

        // Method to add values to the Characteristics (eigenschappen) table
        public void DummyCharacteristics()
        {
            var newChar = new Characteristics() { Type = "Tuin" };
            unitOfWork.CharacteristicsRepo.Add(newChar);

            var newChar2 = new Characteristics() { Type = "Kelder" };
            unitOfWork.CharacteristicsRepo.Add(newChar2);

            var newChar3 = new Characteristics() { Type = "Zolder" };
            unitOfWork.CharacteristicsRepo.Add(newChar3);

            var newChar4 = new Characteristics() { Type = "Garage" };
            unitOfWork.CharacteristicsRepo.Add(newChar4);

            var newChar5 = new Characteristics() { Type = "Zwembad" };
            unitOfWork.CharacteristicsRepo.Add(newChar5);

            var newChar6 = new Characteristics() { Type = "Carport" };
            unitOfWork.CharacteristicsRepo.Add(newChar6);

            var newChar7 = new Characteristics() { Type = "Tennisplein" };
            unitOfWork.CharacteristicsRepo.Add(newChar7);

            var newChar8 = new Characteristics() { Type = "Paardenstallen" };
            unitOfWork.CharacteristicsRepo.Add(newChar8);
            unitOfWork.Commit();
        }

        // Method to add values to the BuyersRenters (kopers_huurders) table
        public void DummyBuyersRenters()
        {
            var newBuyerRenter = new BuyersRenters() { Name = "Longo", Surname = "Lorenzo" };
            unitOfWork.BuyersRentersRepo.Add(newBuyerRenter);

            var newBuyerRenter2 = new BuyersRenters() { Name = "Fransen", Surname = "Frans" };
            unitOfWork.BuyersRentersRepo.Add(newBuyerRenter2);

            var newBuyerRenter3 = new BuyersRenters() { Name = "Duitsen", Surname = "Duits" };
            unitOfWork.BuyersRentersRepo.Add(newBuyerRenter3);
            unitOfWork.Commit();
        }

        // Method to add values to the Agreements inherited tables (overeenkomsten - huurovereenkomsten - verkoopsovereenkomsten)
        public void DummyAgreements()
        {
            var newAgreement = new RentAgreements() { Date = DateTime.Today, Guarantee = 3000.51, MonthlyCost = 659.99, BuyersRenters = dbContext.BuyersRenters.Where(s => s.Id == 1).FirstOrDefault<BuyersRenters>(), Properties = dbContext.Properties.Where(s => s.Id == 2).FirstOrDefault<Properties>() };
            unitOfWork.AgreementsRepo.Add(newAgreement);

            var newAgreement2 = new RentAgreements() { Date = DateTime.Today, Guarantee = 1500.00, MonthlyCost = 550.00, BuyersRenters = dbContext.BuyersRenters.Where(s => s.Id == 3).FirstOrDefault<BuyersRenters>(), Properties = dbContext.Properties.Where(s => s.Id == 6).FirstOrDefault<Properties>() };
            unitOfWork.AgreementsRepo.Add(newAgreement2);

            var newAgreement3 = new BuyAgreements() { Date = DateTime.Today, SellingPrice = 255000.00, BuyersRenters = dbContext.BuyersRenters.Where(s => s.Id == 2).FirstOrDefault<BuyersRenters>(), Properties = dbContext.Properties.Where(s => s.Id == 4).FirstOrDefault<Properties>() };
            unitOfWork.AgreementsRepo.Add(newAgreement3);
            unitOfWork.Commit();
        }

        // Method to connect the many-to-many Characteristics and Buildings values (eigenschappen - panden)
        public void ConnectBuildingsChars()
        {
            using (dbContext)
            {
                var newchar = dbContext.Characteristics.Where(s => s.Type == "Tuin").FirstOrDefault<Characteristics>();
                var newchar2 = dbContext.Characteristics.Where(s => s.Type == "Kelder").FirstOrDefault<Characteristics>();
                var newchar3 = dbContext.Characteristics.Where(s => s.Type == "Zolder").FirstOrDefault<Characteristics>();
                var newchar4 = dbContext.Characteristics.Where(s => s.Type == "Garage").FirstOrDefault<Characteristics>();
                var newchar5 = dbContext.Characteristics.Where(s => s.Type == "Zwembad").FirstOrDefault<Characteristics>();
                var newchar6 = dbContext.Characteristics.Where(s => s.Type == "Carport").FirstOrDefault<Characteristics>();
                var newchar7 = dbContext.Characteristics.Where(s => s.Type == "Tennisplein").FirstOrDefault<Characteristics>();
                var newchar8 = dbContext.Characteristics.Where(s => s.Type == "Paardenstallen").FirstOrDefault<Characteristics>();

                var newProp = dbContext.Buildings.Where(s => s.Id == 2).FirstOrDefault<Buildings>();
                var newProp2 = dbContext.Buildings.Where(s => s.Id == 3).FirstOrDefault<Buildings>();
                var newProp3 = dbContext.Buildings.Where(s => s.Id == 6).FirstOrDefault<Buildings>();

                unitOfWork.BuildingsRepo.Update(newProp);
                unitOfWork.BuildingsRepo.Update(newProp2);
                unitOfWork.BuildingsRepo.Update(newProp3);

                newchar.Buildings.Add(newProp);
                newchar.Buildings.Add(newProp2);

                newchar2.Buildings.Add(newProp3);

                newchar3.Buildings.Add(newProp);
                newchar3.Buildings.Add(newProp3);

                newchar4.Buildings.Add(newProp);
                newchar4.Buildings.Add(newProp3);

                newchar5.Buildings.Add(newProp3);

                newchar6.Buildings.Add(newProp2);

                newchar7.Buildings.Add(newProp3);

                newchar8.Buildings.Add(newProp3);

                unitOfWork.Commit();
            }
        }
    }
}
