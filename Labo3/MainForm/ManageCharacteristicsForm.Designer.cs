﻿namespace MainForm
{
    partial class ManageCharacteristicsForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.lblProperties = new System.Windows.Forms.Label();
            this.lblChars = new System.Windows.Forms.Label();
            this.checkedListBoxChars = new System.Windows.Forms.CheckedListBox();
            this.btnOK = new System.Windows.Forms.Button();
            this.comboBoxProperties = new System.Windows.Forms.ComboBox();
            this.SuspendLayout();
            // 
            // lblProperties
            // 
            this.lblProperties.AutoSize = true;
            this.lblProperties.Location = new System.Drawing.Point(12, 38);
            this.lblProperties.Name = "lblProperties";
            this.lblProperties.Size = new System.Drawing.Size(44, 13);
            this.lblProperties.TabIndex = 0;
            this.lblProperties.Text = "Panden";
            // 
            // lblChars
            // 
            this.lblChars.AutoSize = true;
            this.lblChars.Location = new System.Drawing.Point(12, 97);
            this.lblChars.Name = "lblChars";
            this.lblChars.Size = new System.Drawing.Size(81, 13);
            this.lblChars.TabIndex = 1;
            this.lblChars.Text = "Eigenschappen";
            // 
            // checkedListBoxChars
            // 
            this.checkedListBoxChars.FormattingEnabled = true;
            this.checkedListBoxChars.Location = new System.Drawing.Point(121, 97);
            this.checkedListBoxChars.Name = "checkedListBoxChars";
            this.checkedListBoxChars.Size = new System.Drawing.Size(158, 214);
            this.checkedListBoxChars.TabIndex = 2;
            this.checkedListBoxChars.SelectedValueChanged += new System.EventHandler(this.checkedListBoxChars_SelectedValueChanged);
            // 
            // btnOK
            // 
            this.btnOK.Location = new System.Drawing.Point(330, 288);
            this.btnOK.Name = "btnOK";
            this.btnOK.Size = new System.Drawing.Size(75, 23);
            this.btnOK.TabIndex = 3;
            this.btnOK.Text = "OK";
            this.btnOK.UseVisualStyleBackColor = true;
            this.btnOK.Click += new System.EventHandler(this.btnOK_Click);
            // 
            // comboBoxProperties
            // 
            this.comboBoxProperties.FormattingEnabled = true;
            this.comboBoxProperties.Location = new System.Drawing.Point(121, 35);
            this.comboBoxProperties.Name = "comboBoxProperties";
            this.comboBoxProperties.Size = new System.Drawing.Size(158, 21);
            this.comboBoxProperties.TabIndex = 4;
            this.comboBoxProperties.SelectedIndexChanged += new System.EventHandler(this.comboBoxProperties_SelectedIndexChanged);
            // 
            // ManageCharacteristicsForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(424, 350);
            this.Controls.Add(this.comboBoxProperties);
            this.Controls.Add(this.btnOK);
            this.Controls.Add(this.checkedListBoxChars);
            this.Controls.Add(this.lblChars);
            this.Controls.Add(this.lblProperties);
            this.Name = "ManageCharacteristicsForm";
            this.Text = "Eigenschappen van panden beheren";
            this.FormClosed += new System.Windows.Forms.FormClosedEventHandler(this.ManageCharacteristicsForm_FormClosed);
            this.Load += new System.EventHandler(this.ManageCharacteristicsForm_Load);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label lblProperties;
        private System.Windows.Forms.Label lblChars;
        private System.Windows.Forms.CheckedListBox checkedListBoxChars;
        private System.Windows.Forms.Button btnOK;
        private System.Windows.Forms.ComboBox comboBoxProperties;
    }
}