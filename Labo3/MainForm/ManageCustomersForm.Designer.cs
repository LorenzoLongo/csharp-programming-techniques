﻿namespace MainForm
{
    partial class ManageCustomersForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.groupBoxAddCustomers = new System.Windows.Forms.GroupBox();
            this.btnAddCust = new System.Windows.Forms.Button();
            this.txtBoxName = new System.Windows.Forms.TextBox();
            this.txtBoxSurname = new System.Windows.Forms.TextBox();
            this.lblName = new System.Windows.Forms.Label();
            this.lblSurname = new System.Windows.Forms.Label();
            this.groupBoxDeleteCustomers = new System.Windows.Forms.GroupBox();
            this.btnDeleteCust = new System.Windows.Forms.Button();
            this.comboBoxCustomers = new System.Windows.Forms.ComboBox();
            this.lblDeleteIDText = new System.Windows.Forms.Label();
            this.txtBoxDeleteCustID = new System.Windows.Forms.TextBox();
            this.lblErrorAddCust = new System.Windows.Forms.Label();
            this.lblErrorDelCust = new System.Windows.Forms.Label();
            this.groupBoxAddCustomers.SuspendLayout();
            this.groupBoxDeleteCustomers.SuspendLayout();
            this.SuspendLayout();
            // 
            // groupBoxAddCustomers
            // 
            this.groupBoxAddCustomers.Controls.Add(this.lblErrorAddCust);
            this.groupBoxAddCustomers.Controls.Add(this.btnAddCust);
            this.groupBoxAddCustomers.Controls.Add(this.txtBoxName);
            this.groupBoxAddCustomers.Controls.Add(this.txtBoxSurname);
            this.groupBoxAddCustomers.Controls.Add(this.lblName);
            this.groupBoxAddCustomers.Controls.Add(this.lblSurname);
            this.groupBoxAddCustomers.Location = new System.Drawing.Point(12, 32);
            this.groupBoxAddCustomers.Name = "groupBoxAddCustomers";
            this.groupBoxAddCustomers.Size = new System.Drawing.Size(308, 172);
            this.groupBoxAddCustomers.TabIndex = 0;
            this.groupBoxAddCustomers.TabStop = false;
            this.groupBoxAddCustomers.Text = "Klanten toevoegen";
            // 
            // btnAddCust
            // 
            this.btnAddCust.Location = new System.Drawing.Point(212, 121);
            this.btnAddCust.Name = "btnAddCust";
            this.btnAddCust.Size = new System.Drawing.Size(75, 23);
            this.btnAddCust.TabIndex = 4;
            this.btnAddCust.Text = "Voeg toe";
            this.btnAddCust.UseVisualStyleBackColor = true;
            this.btnAddCust.Click += new System.EventHandler(this.btnAddCust_Click);
            // 
            // txtBoxName
            // 
            this.txtBoxName.Location = new System.Drawing.Point(91, 65);
            this.txtBoxName.Name = "txtBoxName";
            this.txtBoxName.Size = new System.Drawing.Size(196, 20);
            this.txtBoxName.TabIndex = 3;
            // 
            // txtBoxSurname
            // 
            this.txtBoxSurname.Location = new System.Drawing.Point(91, 34);
            this.txtBoxSurname.Name = "txtBoxSurname";
            this.txtBoxSurname.Size = new System.Drawing.Size(196, 20);
            this.txtBoxSurname.TabIndex = 2;
            // 
            // lblName
            // 
            this.lblName.AutoSize = true;
            this.lblName.Location = new System.Drawing.Point(6, 68);
            this.lblName.Name = "lblName";
            this.lblName.Size = new System.Drawing.Size(65, 13);
            this.lblName.TabIndex = 1;
            this.lblName.Text = "Familienaam";
            // 
            // lblSurname
            // 
            this.lblSurname.AutoSize = true;
            this.lblSurname.Location = new System.Drawing.Point(6, 34);
            this.lblSurname.Name = "lblSurname";
            this.lblSurname.Size = new System.Drawing.Size(55, 13);
            this.lblSurname.TabIndex = 0;
            this.lblSurname.Text = "Voornaam";
            // 
            // groupBoxDeleteCustomers
            // 
            this.groupBoxDeleteCustomers.Controls.Add(this.lblErrorDelCust);
            this.groupBoxDeleteCustomers.Controls.Add(this.btnDeleteCust);
            this.groupBoxDeleteCustomers.Controls.Add(this.comboBoxCustomers);
            this.groupBoxDeleteCustomers.Controls.Add(this.lblDeleteIDText);
            this.groupBoxDeleteCustomers.Controls.Add(this.txtBoxDeleteCustID);
            this.groupBoxDeleteCustomers.Location = new System.Drawing.Point(342, 32);
            this.groupBoxDeleteCustomers.Name = "groupBoxDeleteCustomers";
            this.groupBoxDeleteCustomers.Size = new System.Drawing.Size(308, 172);
            this.groupBoxDeleteCustomers.TabIndex = 1;
            this.groupBoxDeleteCustomers.TabStop = false;
            this.groupBoxDeleteCustomers.Text = "Klanten verwijderen";
            // 
            // btnDeleteCust
            // 
            this.btnDeleteCust.Location = new System.Drawing.Point(214, 121);
            this.btnDeleteCust.Name = "btnDeleteCust";
            this.btnDeleteCust.Size = new System.Drawing.Size(75, 23);
            this.btnDeleteCust.TabIndex = 5;
            this.btnDeleteCust.Text = "Verwijder";
            this.btnDeleteCust.UseVisualStyleBackColor = true;
            this.btnDeleteCust.Click += new System.EventHandler(this.btnDeleteCust_Click);
            // 
            // comboBoxCustomers
            // 
            this.comboBoxCustomers.FormattingEnabled = true;
            this.comboBoxCustomers.Location = new System.Drawing.Point(9, 31);
            this.comboBoxCustomers.Name = "comboBoxCustomers";
            this.comboBoxCustomers.Size = new System.Drawing.Size(280, 21);
            this.comboBoxCustomers.TabIndex = 2;
            // 
            // lblDeleteIDText
            // 
            this.lblDeleteIDText.AutoSize = true;
            this.lblDeleteIDText.Location = new System.Drawing.Point(6, 68);
            this.lblDeleteIDText.Name = "lblDeleteIDText";
            this.lblDeleteIDText.Size = new System.Drawing.Size(117, 13);
            this.lblDeleteIDText.TabIndex = 1;
            this.lblDeleteIDText.Text = "Te verwijderen klant ID";
            // 
            // txtBoxDeleteCustID
            // 
            this.txtBoxDeleteCustID.Location = new System.Drawing.Point(231, 68);
            this.txtBoxDeleteCustID.Name = "txtBoxDeleteCustID";
            this.txtBoxDeleteCustID.Size = new System.Drawing.Size(58, 20);
            this.txtBoxDeleteCustID.TabIndex = 0;
            // 
            // lblErrorAddCust
            // 
            this.lblErrorAddCust.AutoSize = true;
            this.lblErrorAddCust.Location = new System.Drawing.Point(6, 126);
            this.lblErrorAddCust.Name = "lblErrorAddCust";
            this.lblErrorAddCust.Size = new System.Drawing.Size(0, 13);
            this.lblErrorAddCust.TabIndex = 5;
            // 
            // lblErrorDelCust
            // 
            this.lblErrorDelCust.AutoSize = true;
            this.lblErrorDelCust.Location = new System.Drawing.Point(6, 126);
            this.lblErrorDelCust.Name = "lblErrorDelCust";
            this.lblErrorDelCust.Size = new System.Drawing.Size(0, 13);
            this.lblErrorDelCust.TabIndex = 6;
            // 
            // ManageCustomersForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(664, 240);
            this.Controls.Add(this.groupBoxDeleteCustomers);
            this.Controls.Add(this.groupBoxAddCustomers);
            this.Name = "ManageCustomersForm";
            this.Text = "Klanten beheren";
            this.FormClosed += new System.Windows.Forms.FormClosedEventHandler(this.ManageCustomersForm_FormClosed);
            this.Load += new System.EventHandler(this.ManageCustomersForm_Load);
            this.groupBoxAddCustomers.ResumeLayout(false);
            this.groupBoxAddCustomers.PerformLayout();
            this.groupBoxDeleteCustomers.ResumeLayout(false);
            this.groupBoxDeleteCustomers.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.GroupBox groupBoxAddCustomers;
        private System.Windows.Forms.Button btnAddCust;
        private System.Windows.Forms.TextBox txtBoxName;
        private System.Windows.Forms.TextBox txtBoxSurname;
        private System.Windows.Forms.Label lblName;
        private System.Windows.Forms.Label lblSurname;
        private System.Windows.Forms.GroupBox groupBoxDeleteCustomers;
        private System.Windows.Forms.Button btnDeleteCust;
        private System.Windows.Forms.ComboBox comboBoxCustomers;
        private System.Windows.Forms.Label lblDeleteIDText;
        private System.Windows.Forms.TextBox txtBoxDeleteCustID;
        private System.Windows.Forms.Label lblErrorAddCust;
        private System.Windows.Forms.Label lblErrorDelCust;
    }
}