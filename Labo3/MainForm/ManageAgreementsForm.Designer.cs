﻿namespace MainForm
{
    partial class ManageAgreementsForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.lblDate = new System.Windows.Forms.Label();
            this.lblProperty = new System.Windows.Forms.Label();
            this.lblBuyersRenters = new System.Windows.Forms.Label();
            this.lblSellPrice = new System.Windows.Forms.Label();
            this.lblEuroSell = new System.Windows.Forms.Label();
            this.lblMontlyPayment = new System.Windows.Forms.Label();
            this.lblEuroCosts = new System.Windows.Forms.Label();
            this.lblCosts = new System.Windows.Forms.Label();
            this.lblGuarantee = new System.Windows.Forms.Label();
            this.lblEuroGuarantee = new System.Windows.Forms.Label();
            this.lblEuroMonthlyPayment = new System.Windows.Forms.Label();
            this.btnStopAgreement = new System.Windows.Forms.Button();
            this.btnAddAgreement = new System.Windows.Forms.Button();
            this.comboBoxProperties = new System.Windows.Forms.ComboBox();
            this.comboBoxBuyersRenters = new System.Windows.Forms.ComboBox();
            this.textBoxSellPrice = new System.Windows.Forms.TextBox();
            this.groupBoxAgreements = new System.Windows.Forms.GroupBox();
            this.lblErrorRent = new System.Windows.Forms.Label();
            this.lblErrorSell = new System.Windows.Forms.Label();
            this.radioBtnRentAgreement = new System.Windows.Forms.RadioButton();
            this.radioBtnSellAgreement = new System.Windows.Forms.RadioButton();
            this.groupBoxRent = new System.Windows.Forms.GroupBox();
            this.textBoxMonthlyPayment = new System.Windows.Forms.TextBox();
            this.textBoxGuarantee = new System.Windows.Forms.TextBox();
            this.groupBoxSell = new System.Windows.Forms.GroupBox();
            this.textBoxCosts = new System.Windows.Forms.TextBox();
            this.dateTimePickerDate = new System.Windows.Forms.DateTimePicker();
            this.lblSucceed = new System.Windows.Forms.Label();
            this.groupBoxAgreements.SuspendLayout();
            this.groupBoxRent.SuspendLayout();
            this.groupBoxSell.SuspendLayout();
            this.SuspendLayout();
            // 
            // lblDate
            // 
            this.lblDate.AutoSize = true;
            this.lblDate.Location = new System.Drawing.Point(21, 29);
            this.lblDate.Name = "lblDate";
            this.lblDate.Size = new System.Drawing.Size(38, 13);
            this.lblDate.TabIndex = 0;
            this.lblDate.Text = "Datum";
            // 
            // lblProperty
            // 
            this.lblProperty.AutoSize = true;
            this.lblProperty.Location = new System.Drawing.Point(21, 61);
            this.lblProperty.Name = "lblProperty";
            this.lblProperty.Size = new System.Drawing.Size(54, 13);
            this.lblProperty.TabIndex = 1;
            this.lblProperty.Text = "Eigendom";
            // 
            // lblBuyersRenters
            // 
            this.lblBuyersRenters.AutoSize = true;
            this.lblBuyersRenters.Location = new System.Drawing.Point(21, 98);
            this.lblBuyersRenters.Name = "lblBuyersRenters";
            this.lblBuyersRenters.Size = new System.Drawing.Size(92, 13);
            this.lblBuyersRenters.TabIndex = 2;
            this.lblBuyersRenters.Text = "Koper/Verhuurder";
            // 
            // lblSellPrice
            // 
            this.lblSellPrice.AutoSize = true;
            this.lblSellPrice.Location = new System.Drawing.Point(6, 16);
            this.lblSellPrice.Name = "lblSellPrice";
            this.lblSellPrice.Size = new System.Drawing.Size(65, 13);
            this.lblSellPrice.TabIndex = 3;
            this.lblSellPrice.Text = "Verkoopprijs";
            // 
            // lblEuroSell
            // 
            this.lblEuroSell.AutoSize = true;
            this.lblEuroSell.Location = new System.Drawing.Point(193, 16);
            this.lblEuroSell.Name = "lblEuroSell";
            this.lblEuroSell.Size = new System.Drawing.Size(28, 13);
            this.lblEuroSell.TabIndex = 4;
            this.lblEuroSell.Text = "euro";
            // 
            // lblMontlyPayment
            // 
            this.lblMontlyPayment.AutoSize = true;
            this.lblMontlyPayment.Location = new System.Drawing.Point(6, 71);
            this.lblMontlyPayment.Name = "lblMontlyPayment";
            this.lblMontlyPayment.Size = new System.Drawing.Size(73, 13);
            this.lblMontlyPayment.TabIndex = 5;
            this.lblMontlyPayment.Text = "Maandbedrag";
            // 
            // lblEuroCosts
            // 
            this.lblEuroCosts.AutoSize = true;
            this.lblEuroCosts.Location = new System.Drawing.Point(201, 16);
            this.lblEuroCosts.Name = "lblEuroCosts";
            this.lblEuroCosts.Size = new System.Drawing.Size(28, 13);
            this.lblEuroCosts.TabIndex = 6;
            this.lblEuroCosts.Text = "euro";
            // 
            // lblCosts
            // 
            this.lblCosts.AutoSize = true;
            this.lblCosts.Location = new System.Drawing.Point(6, 16);
            this.lblCosts.Name = "lblCosts";
            this.lblCosts.Size = new System.Drawing.Size(69, 13);
            this.lblCosts.TabIndex = 6;
            this.lblCosts.Text = "Vaste kosten";
            // 
            // lblGuarantee
            // 
            this.lblGuarantee.AutoSize = true;
            this.lblGuarantee.Location = new System.Drawing.Point(6, 43);
            this.lblGuarantee.Name = "lblGuarantee";
            this.lblGuarantee.Size = new System.Drawing.Size(54, 13);
            this.lblGuarantee.TabIndex = 7;
            this.lblGuarantee.Text = "Waarborg";
            // 
            // lblEuroGuarantee
            // 
            this.lblEuroGuarantee.AutoSize = true;
            this.lblEuroGuarantee.Location = new System.Drawing.Point(201, 43);
            this.lblEuroGuarantee.Name = "lblEuroGuarantee";
            this.lblEuroGuarantee.Size = new System.Drawing.Size(28, 13);
            this.lblEuroGuarantee.TabIndex = 8;
            this.lblEuroGuarantee.Text = "euro";
            // 
            // lblEuroMonthlyPayment
            // 
            this.lblEuroMonthlyPayment.AutoSize = true;
            this.lblEuroMonthlyPayment.Location = new System.Drawing.Point(201, 71);
            this.lblEuroMonthlyPayment.Name = "lblEuroMonthlyPayment";
            this.lblEuroMonthlyPayment.Size = new System.Drawing.Size(28, 13);
            this.lblEuroMonthlyPayment.TabIndex = 9;
            this.lblEuroMonthlyPayment.Text = "euro";
            // 
            // btnStopAgreement
            // 
            this.btnStopAgreement.Location = new System.Drawing.Point(459, 112);
            this.btnStopAgreement.Name = "btnStopAgreement";
            this.btnStopAgreement.Size = new System.Drawing.Size(155, 23);
            this.btnStopAgreement.TabIndex = 10;
            this.btnStopAgreement.Text = "Overeenkomst beëindigen";
            this.btnStopAgreement.UseVisualStyleBackColor = true;
            this.btnStopAgreement.Click += new System.EventHandler(this.btnStopAgreement_Click);
            // 
            // btnAddAgreement
            // 
            this.btnAddAgreement.Location = new System.Drawing.Point(539, 353);
            this.btnAddAgreement.Name = "btnAddAgreement";
            this.btnAddAgreement.Size = new System.Drawing.Size(75, 23);
            this.btnAddAgreement.TabIndex = 11;
            this.btnAddAgreement.Text = "OK";
            this.btnAddAgreement.UseVisualStyleBackColor = true;
            this.btnAddAgreement.Click += new System.EventHandler(this.btnAddAgreement_Click);
            // 
            // comboBoxProperties
            // 
            this.comboBoxProperties.FormattingEnabled = true;
            this.comboBoxProperties.Location = new System.Drawing.Point(131, 61);
            this.comboBoxProperties.Name = "comboBoxProperties";
            this.comboBoxProperties.Size = new System.Drawing.Size(267, 21);
            this.comboBoxProperties.TabIndex = 12;
            // 
            // comboBoxBuyersRenters
            // 
            this.comboBoxBuyersRenters.FormattingEnabled = true;
            this.comboBoxBuyersRenters.Location = new System.Drawing.Point(131, 95);
            this.comboBoxBuyersRenters.Name = "comboBoxBuyersRenters";
            this.comboBoxBuyersRenters.Size = new System.Drawing.Size(267, 21);
            this.comboBoxBuyersRenters.TabIndex = 13;
            // 
            // textBoxSellPrice
            // 
            this.textBoxSellPrice.Location = new System.Drawing.Point(77, 13);
            this.textBoxSellPrice.Name = "textBoxSellPrice";
            this.textBoxSellPrice.Size = new System.Drawing.Size(110, 20);
            this.textBoxSellPrice.TabIndex = 14;
            // 
            // groupBoxAgreements
            // 
            this.groupBoxAgreements.Controls.Add(this.lblErrorRent);
            this.groupBoxAgreements.Controls.Add(this.lblErrorSell);
            this.groupBoxAgreements.Controls.Add(this.radioBtnRentAgreement);
            this.groupBoxAgreements.Controls.Add(this.radioBtnSellAgreement);
            this.groupBoxAgreements.Controls.Add(this.groupBoxRent);
            this.groupBoxAgreements.Controls.Add(this.groupBoxSell);
            this.groupBoxAgreements.Location = new System.Drawing.Point(24, 141);
            this.groupBoxAgreements.Name = "groupBoxAgreements";
            this.groupBoxAgreements.Size = new System.Drawing.Size(590, 197);
            this.groupBoxAgreements.TabIndex = 18;
            this.groupBoxAgreements.TabStop = false;
            // 
            // lblErrorRent
            // 
            this.lblErrorRent.AutoSize = true;
            this.lblErrorRent.Location = new System.Drawing.Point(303, 148);
            this.lblErrorRent.Name = "lblErrorRent";
            this.lblErrorRent.Size = new System.Drawing.Size(0, 13);
            this.lblErrorRent.TabIndex = 23;
            // 
            // lblErrorSell
            // 
            this.lblErrorSell.AutoSize = true;
            this.lblErrorSell.Location = new System.Drawing.Point(26, 100);
            this.lblErrorSell.Name = "lblErrorSell";
            this.lblErrorSell.Size = new System.Drawing.Size(0, 13);
            this.lblErrorSell.TabIndex = 22;
            // 
            // radioBtnRentAgreement
            // 
            this.radioBtnRentAgreement.AutoSize = true;
            this.radioBtnRentAgreement.Location = new System.Drawing.Point(306, 22);
            this.radioBtnRentAgreement.Name = "radioBtnRentAgreement";
            this.radioBtnRentAgreement.Size = new System.Drawing.Size(129, 17);
            this.radioBtnRentAgreement.TabIndex = 21;
            this.radioBtnRentAgreement.Text = "Verhuurovereenkomst";
            this.radioBtnRentAgreement.UseVisualStyleBackColor = true;
            this.radioBtnRentAgreement.CheckedChanged += new System.EventHandler(this.radioBtnRentAgreement_CheckedChanged);
            // 
            // radioBtnSellAgreement
            // 
            this.radioBtnSellAgreement.AutoSize = true;
            this.radioBtnSellAgreement.Checked = true;
            this.radioBtnSellAgreement.Location = new System.Drawing.Point(29, 22);
            this.radioBtnSellAgreement.Name = "radioBtnSellAgreement";
            this.radioBtnSellAgreement.Size = new System.Drawing.Size(137, 17);
            this.radioBtnSellAgreement.TabIndex = 20;
            this.radioBtnSellAgreement.TabStop = true;
            this.radioBtnSellAgreement.Text = "Verkoopsovereenkomst";
            this.radioBtnSellAgreement.UseVisualStyleBackColor = true;
            this.radioBtnSellAgreement.CheckedChanged += new System.EventHandler(this.radioBtnSellAgreement_CheckedChanged);
            // 
            // groupBoxRent
            // 
            this.groupBoxRent.Controls.Add(this.textBoxMonthlyPayment);
            this.groupBoxRent.Controls.Add(this.textBoxCosts);
            this.groupBoxRent.Controls.Add(this.textBoxGuarantee);
            this.groupBoxRent.Controls.Add(this.lblGuarantee);
            this.groupBoxRent.Controls.Add(this.lblCosts);
            this.groupBoxRent.Controls.Add(this.lblMontlyPayment);
            this.groupBoxRent.Controls.Add(this.lblEuroCosts);
            this.groupBoxRent.Controls.Add(this.lblEuroGuarantee);
            this.groupBoxRent.Controls.Add(this.lblEuroMonthlyPayment);
            this.groupBoxRent.Location = new System.Drawing.Point(297, 45);
            this.groupBoxRent.Name = "groupBoxRent";
            this.groupBoxRent.Size = new System.Drawing.Size(261, 100);
            this.groupBoxRent.TabIndex = 19;
            this.groupBoxRent.TabStop = false;
            // 
            // textBoxMonthlyPayment
            // 
            this.textBoxMonthlyPayment.Location = new System.Drawing.Point(85, 68);
            this.textBoxMonthlyPayment.Name = "textBoxMonthlyPayment";
            this.textBoxMonthlyPayment.Size = new System.Drawing.Size(110, 20);
            this.textBoxMonthlyPayment.TabIndex = 17;
            // 
            // textBoxGuarantee
            // 
            this.textBoxGuarantee.Location = new System.Drawing.Point(85, 40);
            this.textBoxGuarantee.Name = "textBoxGuarantee";
            this.textBoxGuarantee.Size = new System.Drawing.Size(110, 20);
            this.textBoxGuarantee.TabIndex = 16;
            // 
            // groupBoxSell
            // 
            this.groupBoxSell.Controls.Add(this.textBoxSellPrice);
            this.groupBoxSell.Controls.Add(this.lblSellPrice);
            this.groupBoxSell.Controls.Add(this.lblEuroSell);
            this.groupBoxSell.Location = new System.Drawing.Point(20, 45);
            this.groupBoxSell.Name = "groupBoxSell";
            this.groupBoxSell.Size = new System.Drawing.Size(239, 52);
            this.groupBoxSell.TabIndex = 18;
            this.groupBoxSell.TabStop = false;
            // 
            // textBoxCosts
            // 
            this.textBoxCosts.Location = new System.Drawing.Point(85, 13);
            this.textBoxCosts.Name = "textBoxCosts";
            this.textBoxCosts.Size = new System.Drawing.Size(110, 20);
            this.textBoxCosts.TabIndex = 15;
            // 
            // dateTimePickerDate
            // 
            this.dateTimePickerDate.Location = new System.Drawing.Point(131, 23);
            this.dateTimePickerDate.Name = "dateTimePickerDate";
            this.dateTimePickerDate.Size = new System.Drawing.Size(267, 20);
            this.dateTimePickerDate.TabIndex = 19;
            // 
            // lblSucceed
            // 
            this.lblSucceed.AutoSize = true;
            this.lblSucceed.Location = new System.Drawing.Point(24, 363);
            this.lblSucceed.Name = "lblSucceed";
            this.lblSucceed.Size = new System.Drawing.Size(0, 13);
            this.lblSucceed.TabIndex = 20;
            // 
            // ManageAgreementsForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(634, 396);
            this.Controls.Add(this.lblSucceed);
            this.Controls.Add(this.dateTimePickerDate);
            this.Controls.Add(this.groupBoxAgreements);
            this.Controls.Add(this.comboBoxBuyersRenters);
            this.Controls.Add(this.comboBoxProperties);
            this.Controls.Add(this.btnAddAgreement);
            this.Controls.Add(this.btnStopAgreement);
            this.Controls.Add(this.lblBuyersRenters);
            this.Controls.Add(this.lblProperty);
            this.Controls.Add(this.lblDate);
            this.Name = "ManageAgreementsForm";
            this.Text = "Overeenkomsten beheren";
            this.FormClosed += new System.Windows.Forms.FormClosedEventHandler(this.ManageAgreementsForm_FormClosed);
            this.Load += new System.EventHandler(this.ManageAgreementsForm_Load);
            this.groupBoxAgreements.ResumeLayout(false);
            this.groupBoxAgreements.PerformLayout();
            this.groupBoxRent.ResumeLayout(false);
            this.groupBoxRent.PerformLayout();
            this.groupBoxSell.ResumeLayout(false);
            this.groupBoxSell.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label lblDate;
        private System.Windows.Forms.Label lblProperty;
        private System.Windows.Forms.Label lblBuyersRenters;
        private System.Windows.Forms.Label lblSellPrice;
        private System.Windows.Forms.Label lblEuroSell;
        private System.Windows.Forms.Label lblMontlyPayment;
        private System.Windows.Forms.Label lblEuroCosts;
        private System.Windows.Forms.Label lblCosts;
        private System.Windows.Forms.Label lblGuarantee;
        private System.Windows.Forms.Label lblEuroGuarantee;
        private System.Windows.Forms.Label lblEuroMonthlyPayment;
        private System.Windows.Forms.Button btnStopAgreement;
        private System.Windows.Forms.Button btnAddAgreement;
        private System.Windows.Forms.ComboBox comboBoxProperties;
        private System.Windows.Forms.ComboBox comboBoxBuyersRenters;
        private System.Windows.Forms.TextBox textBoxSellPrice;
        private System.Windows.Forms.GroupBox groupBoxAgreements;
        private System.Windows.Forms.RadioButton radioBtnRentAgreement;
        private System.Windows.Forms.RadioButton radioBtnSellAgreement;
        private System.Windows.Forms.GroupBox groupBoxRent;
        private System.Windows.Forms.TextBox textBoxMonthlyPayment;
        private System.Windows.Forms.TextBox textBoxGuarantee;
        private System.Windows.Forms.TextBox textBoxCosts;
        private System.Windows.Forms.GroupBox groupBoxSell;
        private System.Windows.Forms.DateTimePicker dateTimePickerDate;
        private System.Windows.Forms.Label lblErrorRent;
        private System.Windows.Forms.Label lblErrorSell;
        private System.Windows.Forms.Label lblSucceed;
    }
}