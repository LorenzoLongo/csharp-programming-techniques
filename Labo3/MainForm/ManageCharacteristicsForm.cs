﻿using Globals.Interfaces;
using System;
using System.Windows.Forms;

namespace MainForm
{
    public partial class ManageCharacteristicsForm : Form
    {
        private ILogic logic;
        public ManageCharacteristicsForm(ILogic logic)
        {
            this.logic = logic;
            InitializeComponent();
        }

        private void ManageCharacteristicsForm_FormClosed(object sender, FormClosedEventArgs e)
        {
            OverviewForm overview = new OverviewForm(logic);
            this.Hide();
            this.Close();
            overview.ShowDialog();
        }

        private void ManageCharacteristicsForm_Load(object sender, EventArgs e)
        {

            foreach (string v in logic.GetCharacteristics())
            {
                checkedListBoxChars.Items.Add(v);
            }
            comboBoxProperties.DataSource = logic.GetProperties();
        }

        private int GetBuildingID()
        {
            return Int32.Parse(comboBoxProperties.Text.Substring(0, 2));
        }


        private void comboBoxProperties_SelectedIndexChanged(object sender, EventArgs e)
        {
            for (int i = 0; i < checkedListBoxChars.Items.Count; i++)
            {
                checkedListBoxChars.SetItemChecked(i, false);
            }
            foreach (int v in logic.GetCheckedChars(GetBuildingID()))
            {
                checkedListBoxChars.SetItemChecked((v - 1), true);
            }
        }

        private void checkedListBoxChars_SelectedValueChanged(object sender, EventArgs e)
        {
            var status = checkedListBoxChars.GetItemCheckState(checkedListBoxChars.SelectedIndex);
            if (status.ToString() == "Checked") logic.UpdateCheckedChars(checkedListBoxChars.SelectedIndex, GetBuildingID(), true);
            else if (status.ToString() == "Unchecked") logic.UpdateCheckedChars(checkedListBoxChars.SelectedIndex, GetBuildingID(), false);
        }

        private void btnOK_Click(object sender, EventArgs e)
        {
            OverviewForm overview = new OverviewForm(logic);
            this.Hide();
            this.Close();
            overview.ShowDialog();
        }
    }
}
