﻿namespace MainForm
{
    partial class ManagePropertiesForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.lblStreet = new System.Windows.Forms.Label();
            this.lblTown = new System.Windows.Forms.Label();
            this.lblSurface = new System.Windows.Forms.Label();
            this.lblGroundType = new System.Windows.Forms.Label();
            this.lblBuildingsType = new System.Windows.Forms.Label();
            this.lblRooms = new System.Windows.Forms.Label();
            this.lblHouseNr = new System.Windows.Forms.Label();
            this.lblBoxNr = new System.Windows.Forms.Label();
            this.lblEPC = new System.Windows.Forms.Label();
            this.groupBoxProperties = new System.Windows.Forms.GroupBox();
            this.radioBtnBuildings = new System.Windows.Forms.RadioButton();
            this.radioBtnGrounds = new System.Windows.Forms.RadioButton();
            this.groupBoxBuildings = new System.Windows.Forms.GroupBox();
            this.groupBoxGrounds = new System.Windows.Forms.GroupBox();
            this.comboBoxGroundsType = new System.Windows.Forms.ComboBox();
            this.textBoxSurface = new System.Windows.Forms.TextBox();
            this.textBoxStreet = new System.Windows.Forms.TextBox();
            this.textBoxTown = new System.Windows.Forms.TextBox();
            this.textBoxRooms = new System.Windows.Forms.TextBox();
            this.textBoxHouseNr = new System.Windows.Forms.TextBox();
            this.textBoxBusNr = new System.Windows.Forms.TextBox();
            this.textBoxEPC = new System.Windows.Forms.TextBox();
            this.comboBoxBuildingsType = new System.Windows.Forms.ComboBox();
            this.btnAddProperty = new System.Windows.Forms.Button();
            this.lblErrorBuildings = new System.Windows.Forms.Label();
            this.lblErrorGrounds = new System.Windows.Forms.Label();
            this.lblPropertyAdded = new System.Windows.Forms.Label();
            this.groupBoxProperties.SuspendLayout();
            this.groupBoxBuildings.SuspendLayout();
            this.groupBoxGrounds.SuspendLayout();
            this.SuspendLayout();
            // 
            // lblStreet
            // 
            this.lblStreet.AutoSize = true;
            this.lblStreet.Location = new System.Drawing.Point(35, 35);
            this.lblStreet.Name = "lblStreet";
            this.lblStreet.Size = new System.Drawing.Size(35, 13);
            this.lblStreet.TabIndex = 0;
            this.lblStreet.Text = "Straat";
            // 
            // lblTown
            // 
            this.lblTown.AutoSize = true;
            this.lblTown.Location = new System.Drawing.Point(35, 77);
            this.lblTown.Name = "lblTown";
            this.lblTown.Size = new System.Drawing.Size(56, 13);
            this.lblTown.TabIndex = 1;
            this.lblTown.Text = "Gemeente";
            // 
            // lblSurface
            // 
            this.lblSurface.AutoSize = true;
            this.lblSurface.Location = new System.Drawing.Point(6, 29);
            this.lblSurface.Name = "lblSurface";
            this.lblSurface.Size = new System.Drawing.Size(65, 13);
            this.lblSurface.TabIndex = 2;
            this.lblSurface.Text = "Oppervlakte";
            // 
            // lblGroundType
            // 
            this.lblGroundType.AutoSize = true;
            this.lblGroundType.Location = new System.Drawing.Point(6, 65);
            this.lblGroundType.Name = "lblGroundType";
            this.lblGroundType.Size = new System.Drawing.Size(31, 13);
            this.lblGroundType.TabIndex = 3;
            this.lblGroundType.Text = "Type";
            // 
            // lblBuildingsType
            // 
            this.lblBuildingsType.AutoSize = true;
            this.lblBuildingsType.Location = new System.Drawing.Point(6, 29);
            this.lblBuildingsType.Name = "lblBuildingsType";
            this.lblBuildingsType.Size = new System.Drawing.Size(31, 13);
            this.lblBuildingsType.TabIndex = 4;
            this.lblBuildingsType.Text = "Type";
            // 
            // lblRooms
            // 
            this.lblRooms.AutoSize = true;
            this.lblRooms.Location = new System.Drawing.Point(6, 65);
            this.lblRooms.Name = "lblRooms";
            this.lblRooms.Size = new System.Drawing.Size(42, 13);
            this.lblRooms.TabIndex = 5;
            this.lblRooms.Text = "Kamers";
            // 
            // lblHouseNr
            // 
            this.lblHouseNr.AutoSize = true;
            this.lblHouseNr.Location = new System.Drawing.Point(6, 98);
            this.lblHouseNr.Name = "lblHouseNr";
            this.lblHouseNr.Size = new System.Drawing.Size(42, 13);
            this.lblHouseNr.TabIndex = 6;
            this.lblHouseNr.Text = "Huis Nr";
            // 
            // lblBoxNr
            // 
            this.lblBoxNr.AutoSize = true;
            this.lblBoxNr.Location = new System.Drawing.Point(7, 132);
            this.lblBoxNr.Name = "lblBoxNr";
            this.lblBoxNr.Size = new System.Drawing.Size(39, 13);
            this.lblBoxNr.TabIndex = 7;
            this.lblBoxNr.Text = "Bus Nr";
            // 
            // lblEPC
            // 
            this.lblEPC.AutoSize = true;
            this.lblEPC.Location = new System.Drawing.Point(9, 163);
            this.lblEPC.Name = "lblEPC";
            this.lblEPC.Size = new System.Drawing.Size(28, 13);
            this.lblEPC.TabIndex = 8;
            this.lblEPC.Text = "EPC";
            // 
            // groupBoxProperties
            // 
            this.groupBoxProperties.Controls.Add(this.lblErrorBuildings);
            this.groupBoxProperties.Controls.Add(this.lblErrorGrounds);
            this.groupBoxProperties.Controls.Add(this.radioBtnBuildings);
            this.groupBoxProperties.Controls.Add(this.radioBtnGrounds);
            this.groupBoxProperties.Controls.Add(this.groupBoxBuildings);
            this.groupBoxProperties.Controls.Add(this.groupBoxGrounds);
            this.groupBoxProperties.Location = new System.Drawing.Point(25, 111);
            this.groupBoxProperties.Name = "groupBoxProperties";
            this.groupBoxProperties.Size = new System.Drawing.Size(576, 284);
            this.groupBoxProperties.TabIndex = 9;
            this.groupBoxProperties.TabStop = false;
            // 
            // radioBtnBuildings
            // 
            this.radioBtnBuildings.AutoSize = true;
            this.radioBtnBuildings.Checked = true;
            this.radioBtnBuildings.Location = new System.Drawing.Point(339, 19);
            this.radioBtnBuildings.Name = "radioBtnBuildings";
            this.radioBtnBuildings.Size = new System.Drawing.Size(62, 17);
            this.radioBtnBuildings.TabIndex = 3;
            this.radioBtnBuildings.TabStop = true;
            this.radioBtnBuildings.Text = "Panden";
            this.radioBtnBuildings.UseVisualStyleBackColor = true;
            this.radioBtnBuildings.CheckedChanged += new System.EventHandler(this.radioBtnBuildings_CheckedChanged);
            // 
            // radioBtnGrounds
            // 
            this.radioBtnGrounds.AutoSize = true;
            this.radioBtnGrounds.Location = new System.Drawing.Point(13, 19);
            this.radioBtnGrounds.Name = "radioBtnGrounds";
            this.radioBtnGrounds.Size = new System.Drawing.Size(66, 17);
            this.radioBtnGrounds.TabIndex = 2;
            this.radioBtnGrounds.Text = "Gronden";
            this.radioBtnGrounds.UseVisualStyleBackColor = true;
            this.radioBtnGrounds.CheckedChanged += new System.EventHandler(this.radioBtnGrounds_CheckedChanged);
            // 
            // groupBoxBuildings
            // 
            this.groupBoxBuildings.Controls.Add(this.comboBoxBuildingsType);
            this.groupBoxBuildings.Controls.Add(this.textBoxEPC);
            this.groupBoxBuildings.Controls.Add(this.textBoxBusNr);
            this.groupBoxBuildings.Controls.Add(this.textBoxHouseNr);
            this.groupBoxBuildings.Controls.Add(this.textBoxRooms);
            this.groupBoxBuildings.Controls.Add(this.lblBuildingsType);
            this.groupBoxBuildings.Controls.Add(this.lblEPC);
            this.groupBoxBuildings.Controls.Add(this.lblRooms);
            this.groupBoxBuildings.Controls.Add(this.lblHouseNr);
            this.groupBoxBuildings.Controls.Add(this.lblBoxNr);
            this.groupBoxBuildings.Location = new System.Drawing.Point(348, 52);
            this.groupBoxBuildings.Name = "groupBoxBuildings";
            this.groupBoxBuildings.Size = new System.Drawing.Size(206, 200);
            this.groupBoxBuildings.TabIndex = 1;
            this.groupBoxBuildings.TabStop = false;
            // 
            // groupBoxGrounds
            // 
            this.groupBoxGrounds.Controls.Add(this.comboBoxGroundsType);
            this.groupBoxGrounds.Controls.Add(this.textBoxSurface);
            this.groupBoxGrounds.Controls.Add(this.lblSurface);
            this.groupBoxGrounds.Controls.Add(this.lblGroundType);
            this.groupBoxGrounds.Location = new System.Drawing.Point(22, 52);
            this.groupBoxGrounds.Name = "groupBoxGrounds";
            this.groupBoxGrounds.Size = new System.Drawing.Size(239, 100);
            this.groupBoxGrounds.TabIndex = 0;
            this.groupBoxGrounds.TabStop = false;
            // 
            // comboBoxGroundsType
            // 
            this.comboBoxGroundsType.FormattingEnabled = true;
            this.comboBoxGroundsType.Location = new System.Drawing.Point(94, 65);
            this.comboBoxGroundsType.Name = "comboBoxGroundsType";
            this.comboBoxGroundsType.Size = new System.Drawing.Size(130, 21);
            this.comboBoxGroundsType.TabIndex = 5;
            // 
            // textBoxSurface
            // 
            this.textBoxSurface.Location = new System.Drawing.Point(94, 26);
            this.textBoxSurface.Name = "textBoxSurface";
            this.textBoxSurface.Size = new System.Drawing.Size(130, 20);
            this.textBoxSurface.TabIndex = 4;
            // 
            // textBoxStreet
            // 
            this.textBoxStreet.Location = new System.Drawing.Point(125, 35);
            this.textBoxStreet.Name = "textBoxStreet";
            this.textBoxStreet.Size = new System.Drawing.Size(269, 20);
            this.textBoxStreet.TabIndex = 10;
            // 
            // textBoxTown
            // 
            this.textBoxTown.Location = new System.Drawing.Point(125, 74);
            this.textBoxTown.Name = "textBoxTown";
            this.textBoxTown.Size = new System.Drawing.Size(269, 20);
            this.textBoxTown.TabIndex = 11;
            // 
            // textBoxRooms
            // 
            this.textBoxRooms.Location = new System.Drawing.Point(64, 62);
            this.textBoxRooms.Name = "textBoxRooms";
            this.textBoxRooms.Size = new System.Drawing.Size(130, 20);
            this.textBoxRooms.TabIndex = 6;
            // 
            // textBoxHouseNr
            // 
            this.textBoxHouseNr.Location = new System.Drawing.Point(64, 95);
            this.textBoxHouseNr.Name = "textBoxHouseNr";
            this.textBoxHouseNr.Size = new System.Drawing.Size(130, 20);
            this.textBoxHouseNr.TabIndex = 9;
            // 
            // textBoxBusNr
            // 
            this.textBoxBusNr.Location = new System.Drawing.Point(64, 129);
            this.textBoxBusNr.Name = "textBoxBusNr";
            this.textBoxBusNr.Size = new System.Drawing.Size(130, 20);
            this.textBoxBusNr.TabIndex = 10;
            // 
            // textBoxEPC
            // 
            this.textBoxEPC.Location = new System.Drawing.Point(64, 160);
            this.textBoxEPC.Name = "textBoxEPC";
            this.textBoxEPC.Size = new System.Drawing.Size(130, 20);
            this.textBoxEPC.TabIndex = 11;
            // 
            // comboBoxBuildingsType
            // 
            this.comboBoxBuildingsType.FormattingEnabled = true;
            this.comboBoxBuildingsType.Location = new System.Drawing.Point(64, 26);
            this.comboBoxBuildingsType.Name = "comboBoxBuildingsType";
            this.comboBoxBuildingsType.Size = new System.Drawing.Size(130, 21);
            this.comboBoxBuildingsType.TabIndex = 6;
            // 
            // btnAddProperty
            // 
            this.btnAddProperty.Location = new System.Drawing.Point(38, 401);
            this.btnAddProperty.Name = "btnAddProperty";
            this.btnAddProperty.Size = new System.Drawing.Size(114, 23);
            this.btnAddProperty.TabIndex = 12;
            this.btnAddProperty.Text = "Voeg toe";
            this.btnAddProperty.UseVisualStyleBackColor = true;
            this.btnAddProperty.Click += new System.EventHandler(this.btnAddProperty_Click);
            // 
            // lblErrorBuildings
            // 
            this.lblErrorBuildings.AutoSize = true;
            this.lblErrorBuildings.Location = new System.Drawing.Point(336, 256);
            this.lblErrorBuildings.Name = "lblErrorBuildings";
            this.lblErrorBuildings.Size = new System.Drawing.Size(0, 13);
            this.lblErrorBuildings.TabIndex = 15;
            // 
            // lblErrorGrounds
            // 
            this.lblErrorGrounds.AutoSize = true;
            this.lblErrorGrounds.Location = new System.Drawing.Point(28, 165);
            this.lblErrorGrounds.Name = "lblErrorGrounds";
            this.lblErrorGrounds.Size = new System.Drawing.Size(0, 13);
            this.lblErrorGrounds.TabIndex = 16;
            // 
            // lblPropertyAdded
            // 
            this.lblPropertyAdded.AutoSize = true;
            this.lblPropertyAdded.Location = new System.Drawing.Point(176, 406);
            this.lblPropertyAdded.Name = "lblPropertyAdded";
            this.lblPropertyAdded.Size = new System.Drawing.Size(0, 13);
            this.lblPropertyAdded.TabIndex = 13;
            // 
            // ManagePropertiesForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(618, 447);
            this.Controls.Add(this.lblPropertyAdded);
            this.Controls.Add(this.btnAddProperty);
            this.Controls.Add(this.textBoxTown);
            this.Controls.Add(this.textBoxStreet);
            this.Controls.Add(this.groupBoxProperties);
            this.Controls.Add(this.lblTown);
            this.Controls.Add(this.lblStreet);
            this.Name = "ManagePropertiesForm";
            this.Text = "Eigendommen beheren";
            this.FormClosed += new System.Windows.Forms.FormClosedEventHandler(this.ManagePropertiesForm_FormClosed);
            this.Load += new System.EventHandler(this.ManagePropertiesForm_Load);
            this.groupBoxProperties.ResumeLayout(false);
            this.groupBoxProperties.PerformLayout();
            this.groupBoxBuildings.ResumeLayout(false);
            this.groupBoxBuildings.PerformLayout();
            this.groupBoxGrounds.ResumeLayout(false);
            this.groupBoxGrounds.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label lblStreet;
        private System.Windows.Forms.Label lblTown;
        private System.Windows.Forms.Label lblSurface;
        private System.Windows.Forms.Label lblGroundType;
        private System.Windows.Forms.Label lblBuildingsType;
        private System.Windows.Forms.Label lblRooms;
        private System.Windows.Forms.Label lblHouseNr;
        private System.Windows.Forms.Label lblBoxNr;
        private System.Windows.Forms.Label lblEPC;
        private System.Windows.Forms.GroupBox groupBoxProperties;
        private System.Windows.Forms.RadioButton radioBtnBuildings;
        private System.Windows.Forms.RadioButton radioBtnGrounds;
        private System.Windows.Forms.GroupBox groupBoxBuildings;
        private System.Windows.Forms.GroupBox groupBoxGrounds;
        private System.Windows.Forms.ComboBox comboBoxGroundsType;
        private System.Windows.Forms.TextBox textBoxSurface;
        private System.Windows.Forms.TextBox textBoxStreet;
        private System.Windows.Forms.TextBox textBoxTown;
        private System.Windows.Forms.ComboBox comboBoxBuildingsType;
        private System.Windows.Forms.TextBox textBoxEPC;
        private System.Windows.Forms.TextBox textBoxBusNr;
        private System.Windows.Forms.TextBox textBoxHouseNr;
        private System.Windows.Forms.TextBox textBoxRooms;
        private System.Windows.Forms.Button btnAddProperty;
        private System.Windows.Forms.Label lblErrorBuildings;
        private System.Windows.Forms.Label lblErrorGrounds;
        private System.Windows.Forms.Label lblPropertyAdded;
    }
}