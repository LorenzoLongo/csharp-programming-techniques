﻿using Globals.Interfaces;
using System;
using System.Drawing;
using System.Windows.Forms;

namespace MainForm
{
    public partial class ManagePropertiesForm : Form
    {
        private ILogic logic;

        // Constructor
        public ManagePropertiesForm(ILogic logic)
        {
            this.logic = logic;
            InitializeComponent();
        }

        // Initialize components
        private void ManagePropertiesForm_Load(object sender, EventArgs e)
        {
            if (radioBtnBuildings.Checked) groupBoxGrounds.Enabled = false;
            comboBoxBuildingsType.DataSource = logic.GetBuildingTypes();
        }

        // Method that interacts when the form is closed
        private void ManagePropertiesForm_FormClosed(object sender, FormClosedEventArgs e)
        {
            OverviewForm overview = new OverviewForm(logic);
            this.Hide();
            this.Close();
            overview.ShowDialog();
        }


        private void radioBtnGrounds_CheckedChanged(object sender, EventArgs e)
        {
            groupBoxBuildings.Enabled = false;
            groupBoxGrounds.Enabled = true;
            lblErrorBuildings.Text = "";
            comboBoxGroundsType.DataSource = logic.GetGroundTypes();
        }

        private void radioBtnBuildings_CheckedChanged(object sender, EventArgs e)
        {
            groupBoxBuildings.Enabled = true;
            groupBoxGrounds.Enabled = false;
            lblErrorGrounds.Text = "";
            comboBoxBuildingsType.DataSource = logic.GetBuildingTypes();
        }

        private void btnAddProperty_Click(object sender, EventArgs e)
        {
            if (radioBtnBuildings.Checked)
            {
                AddBuildingsSelected();
            }
            else if (radioBtnGrounds.Checked)
            {
                AddGroundsSelected();
            }
        }
        private void AddBuildingsSelected()
        {
            if (string.IsNullOrWhiteSpace(textBoxStreet.Text) || string.IsNullOrWhiteSpace(textBoxTown.Text) || string.IsNullOrWhiteSpace(textBoxHouseNr.Text))
            {
                lblErrorBuildings.Text = "Vul de velden straat, gemeente, type en huisnr in!";
                lblErrorBuildings.ForeColor = Color.Red;
            }
            else
            {
                lblErrorBuildings.Text = lblErrorGrounds.Text = lblPropertyAdded.Text = "";
                lblPropertyAdded.Text = "Eigendom is toegevoegd!";
                lblPropertyAdded.ForeColor = Color.Green;
                OverloadedBuildings();

            }
        }

        private void AddGroundsSelected()
        {
            if (string.IsNullOrWhiteSpace(textBoxStreet.Text) || string.IsNullOrWhiteSpace(textBoxTown.Text))
            {
                lblErrorGrounds.Text = "Vul de velden straat, gemeente en type in!";
                lblErrorGrounds.ForeColor = Color.Red;
            }
            else
            {
                lblErrorBuildings.Text = lblErrorGrounds.Text = lblPropertyAdded.Text = "";
                lblPropertyAdded.Text = "Eigendom is toegevoegd!";
                lblPropertyAdded.ForeColor = Color.Green;
                OverloadedGrounds();
            }
        }

        private void OverloadedBuildings()
        {
            if (string.IsNullOrEmpty(textBoxRooms.Text) && string.IsNullOrEmpty(textBoxBusNr.Text) && string.IsNullOrEmpty(textBoxEPC.Text))
                logic.AddBuilding(textBoxTown.Text, textBoxStreet.Text, comboBoxBuildingsType.SelectedItem, Int32.Parse(textBoxHouseNr.Text));
            else if (string.IsNullOrEmpty(textBoxBusNr.Text) && string.IsNullOrEmpty(textBoxEPC.Text))
                logic.AddBuilding(textBoxTown.Text, textBoxStreet.Text, comboBoxBuildingsType.SelectedItem, Int32.Parse(textBoxRooms.Text), Int32.Parse(textBoxHouseNr.Text), true);
            else if (string.IsNullOrEmpty(textBoxBusNr.Text) && string.IsNullOrEmpty(textBoxRooms.Text))
                logic.AddBuilding(textBoxTown.Text, textBoxStreet.Text, comboBoxBuildingsType.SelectedItem, Int32.Parse(textBoxHouseNr.Text), double.Parse(textBoxEPC.Text));
            else if (string.IsNullOrEmpty(textBoxRooms.Text) && string.IsNullOrEmpty(textBoxEPC.Text))
                logic.AddBuilding(textBoxTown.Text, textBoxStreet.Text, comboBoxBuildingsType.SelectedItem, Int32.Parse(textBoxBusNr.Text), Int32.Parse(textBoxHouseNr.Text), false);
            else if (string.IsNullOrEmpty(textBoxRooms.Text))
                logic.AddBuilding(textBoxTown.Text, textBoxStreet.Text, comboBoxBuildingsType.SelectedItem, Int32.Parse(textBoxBusNr.Text), Int32.Parse(textBoxHouseNr.Text), double.Parse(textBoxEPC.Text), false);
            else if (string.IsNullOrEmpty(textBoxBusNr.Text))
                logic.AddBuilding(textBoxTown.Text, textBoxStreet.Text, comboBoxBuildingsType.SelectedItem, Int32.Parse(textBoxRooms.Text), Int32.Parse(textBoxHouseNr.Text), double.Parse(textBoxEPC.Text), true);
            else if (string.IsNullOrEmpty(textBoxEPC.Text))
                logic.AddBuilding(textBoxTown.Text, textBoxStreet.Text, comboBoxBuildingsType.SelectedItem, Int32.Parse(textBoxRooms.Text), Int32.Parse(textBoxHouseNr.Text), Int32.Parse(textBoxBusNr.Text));
            else
                logic.AddBuilding(textBoxTown.Text, textBoxStreet.Text, comboBoxBuildingsType.SelectedItem, Int32.Parse(textBoxRooms.Text), Int32.Parse(textBoxHouseNr.Text), Int32.Parse(textBoxBusNr.Text), double.Parse(textBoxEPC.Text));
        }

        private void OverloadedGrounds()
        {
            if (string.IsNullOrEmpty(textBoxSurface.Text))
                logic.AddGround(textBoxTown.Text, textBoxStreet.Text, comboBoxGroundsType.SelectedItem);
            else
                logic.AddGround(textBoxTown.Text, textBoxStreet.Text, double.Parse(textBoxSurface.Text), comboBoxGroundsType.SelectedItem);
        }
    }
}
