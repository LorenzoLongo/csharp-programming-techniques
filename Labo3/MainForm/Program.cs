﻿using DataLayer.Interfaces;
using DataLayer.Patterns;
using Globals.Interfaces;
using LogicLayer;
using System;
using System.Windows.Forms;

namespace MainForm
{
    static class Program
    {
        /// <summary>
        /// The main entry point for the application.
        /// </summary>
        [STAThread]
        static void Main()
        {
            MapDBContext dBContext = new MapDBContext("server=localhost; uid=root; password=; database=CodeFirst;");
            IUnitOfWork unit = new UnitOfWork(dBContext);
            ILogic logic = new Logic(unit, dBContext);
            IDummyValues values = new DummyValues(unit, dBContext);
            Application.EnableVisualStyles();
            Application.SetCompatibleTextRenderingDefault(false);
            Application.Run(new OverviewForm(logic, values));
        }
    }
}
