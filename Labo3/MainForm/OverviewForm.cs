﻿using Globals.Interfaces;
using System;
using System.Windows.Forms;

namespace MainForm
{
    // Class which interacts with the user
    public partial class OverviewForm : Form
    {
        private ILogic logic;
        private IDummyValues values;

        // Constructor
        public OverviewForm(ILogic logic)
        {
            this.logic = logic;
            InitializeComponent();
        }

        // Constructor overload
        public OverviewForm(ILogic logic, IDummyValues values)
        {
            this.logic = logic;
            this.values = values;
            InitializeComponent();
        }

        // Initialize components and fill database
        private void Form1_Load(object sender, EventArgs e)
        {
            //values.DummyProperties();
            //values.DummyCharacteristics();
            //values.DummyBuyersRenters();
            //values.DummyAgreements();

            // !!!!!!! Eerste runnen zonder deze methode !!!!!!!!
            //values.ConnectBuildingsChars();
        }

        // Method to open the Customers Form
        private void btnCustomers_Click(object sender, EventArgs e)
        {
            this.Hide();
            ManageCustomersForm customers = new ManageCustomersForm(logic);
            customers.ShowDialog();
        }

        // Method to close the application
        private void OverviewForm_FormClosed(object sender, FormClosedEventArgs e)
        {
            Application.Exit();
        }

        // Method to open the Properties Form
        private void btnProperties_Click(object sender, EventArgs e)
        {
            this.Hide();
            ManagePropertiesForm properties = new ManagePropertiesForm(logic);
            properties.ShowDialog();
        }

        // Method to open the Characteristics Form
        private void btnCharacteristics_Click(object sender, EventArgs e)
        {
            this.Hide();
            ManageCharacteristicsForm chars = new ManageCharacteristicsForm(logic);
            chars.ShowDialog();
        }

        // Method to open the Agreements Form
        private void btnAgreements_Click(object sender, EventArgs e)
        {
            this.Hide();
            ManageAgreementsForm agreements = new ManageAgreementsForm(logic);
            agreements.ShowDialog();
        }
    }
}
