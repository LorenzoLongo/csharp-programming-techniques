﻿using Globals.Interfaces;
using System;
using System.Drawing;
using System.Windows.Forms;

namespace MainForm
{
    // Class which interacts with the user to stop an agreement
    public partial class StopAgreementForm : Form
    {
        private readonly ILogic logic;

        // Constructor
        public StopAgreementForm(ILogic logic)
        {
            this.logic = logic;
            InitializeComponent();
        }

        // Method which acts when the form is closed
        private void StopAgreementForm_FormClosed(object sender, FormClosedEventArgs e)
        {
            OverviewForm overview = new OverviewForm(logic);
            this.Hide();
            this.Close();
            overview.ShowDialog();
        }

        // Initialize components
        private void StopAgreementForm_Load(object sender, EventArgs e)
        {
            comboBoxProperties.DataSource = logic.GetAgreements();
        }

        // Method to stop an agreement
        private void btnStopAgreement_Click(object sender, EventArgs e)
        {
            logic.TerminateAgreement(GetPropertyID());

            comboBoxProperties.DataSource = logic.GetAgreements();
            lblSuceed.Text = "Overeenkomst verbroken!";
            lblSuceed.ForeColor = Color.Green;
        }

        // Method to get the combobox selected property ID
        private int GetPropertyID()
        {
            return Int32.Parse(comboBoxProperties.Text.Substring(0, 2));
        }

        // Method to set if the property is a ground or a building
        private void comboBoxProperties_SelectedIndexChanged(object sender, EventArgs e)
        {
            var boolean = logic.GroundOrBuilding(GetPropertyID());

            if (boolean == true) radioBtnGrounds.Checked = true;
            else if (boolean == false) radioBtnBuildings.Checked = true;
        }
    }
}
