﻿using System.Collections.Generic;

namespace Globals.Interfaces
{
    // Interface of Player class
    public interface IPlayer
    {
        List<int> AddScoreForGame();
        void ScoresOfGamesSorted();
        List<int> GetAverageTopScores();
        int MaxScoreForGame();
    }
}
