﻿using System.Data;

namespace Globals.Interfaces
{
    // Interface for the Connection class
    public interface IConnection
    {
        DataSet Set { get; }

        void OpenConnection();
        void CloseConnection();
        void SetPlayersAndGamesName();
        void GetAllGameScores(int selectedGame);
        void GetTopScorePerGame();
        void GetPlayerScores(int selectedPlayer);
    }
}
