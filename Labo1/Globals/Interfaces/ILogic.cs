﻿using System.Collections.Generic;

namespace Globals.Interfaces
{
    // Interface of Logic class
    public interface ILogic
    {
        List<string> PlayerNamesAVG { get; }
        List<string> GameNames { get; }
        List<string> PlayerTSList { get; }
        List<string> AllScores { get; }
        List<string> TopScorePerGame { get; }

        int SelectedGameIndex { get; set; }
        int SelectedPlayerIndex { get; set; }

        void GetPlayersAndGames();
        void AllScoresForGame();
        void PlayerTopScores();
    }
}
