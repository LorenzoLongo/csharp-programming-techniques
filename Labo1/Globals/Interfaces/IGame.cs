﻿using System.Collections.Generic;

namespace Globals.Interfaces
{
    // Interface of Game class
    public interface IGame
    {
        List<string> AllTimeTopScore { get; }

        void GetTopscoreAllGames(IConnection conn);
    }
}
