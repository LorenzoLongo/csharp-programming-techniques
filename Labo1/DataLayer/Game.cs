﻿using Globals.Interfaces;
using System.Collections.Generic;
using System.Data;
using System.Linq;

namespace DataLayer
{
    // Class which contains gamedata
    public class Game : IGame
    {
        // Sets all the overall topscores of all games
        public List<string> AllTimeTopScore { get; private set; }

        // Metod to get all the topscore per game
        public void GetTopscoreAllGames(IConnection conn)
        {
            conn.GetTopScorePerGame();
            AllTimeTopScore = conn.Set.Tables[0].AsEnumerable().Select(r => r.Field<int>("max(scores.score)")).ToList().ConvertAll<string>(x => x.ToString());
        }
    }
}
