﻿using Globals.Interfaces;
using MySql.Data.MySqlClient;
using System.Data;

namespace DataLayer
{
    // Class which contains the queries and connection with the database
    public class Connection : IConnection
    {
        // Auto-implemented property 
        public DataSet Set { get; private set; }

        // private fields
        private MySqlConnection conn;
        private MySqlDataAdapter dataAdapter;

        // Opens the connection with the database, if remote fails, take localhost
        public void OpenConnection()
        {
            try
            {
                conn = new MySqlConnection("SERVER=10.129.28.180;DATABASE=r0476338_games_scores;UID=r0476338_root;PASSWORD=Azerty123!;");
                conn.Open();
            }
            catch
            {
                conn = new MySqlConnection("SERVER=localhost;DATABASE=scores;UID=root;PASSWORD=;");
                conn.Open();
            }
            finally
            {
                conn.Close();
            }
        }

        // Closes the connection with the database
        public void CloseConnection()
        {
            conn.Close();
        }

        // Method to set the players and games names
        public void SetPlayersAndGamesName()
        {
            // Initialize DataSet
            Set = new DataSet();

            // Fill Table with the names of the players, with their average value
            for (int i = 1; i <= 4; i++)
            {
                dataAdapter = new MySqlDataAdapter(" select players.name, avg(scores.score) from (players inner join scores on players.id = scores.players_id) where players.id =" + i + ";", conn);
                MySqlCommandBuilder cmd = new MySqlCommandBuilder(dataAdapter);
                dataAdapter.Fill(Set, "PlayerNames");
            }

            // Fill Table with the names of the games
            dataAdapter = new MySqlDataAdapter("select name from games;", conn);
            MySqlCommandBuilder cmd2 = new MySqlCommandBuilder(dataAdapter);
            dataAdapter.Fill(Set, "GameNames");
        }

        // Method which provides all scores for a selected game
        public void GetAllGameScores(int selectedGame)
        {
            Set = new DataSet();
            if (selectedGame == 0)
            {
                dataAdapter = new MySqlDataAdapter("select players.nickname, scores.score from((players inner join scores on players.id = scores.players_id) inner join games on scores.games_id = games.id) where games.name = \"Super Mario\" order by scores.score desc;", conn);
            }
            else if (selectedGame == 1)
            {
                dataAdapter = new MySqlDataAdapter("select players.nickname, scores.score from((players inner join scores on players.id = scores.players_id) inner join games on scores.games_id = games.id) where games.name = \"GTA\" order by scores.score desc;", conn);
            }
            else if (selectedGame == 2)
            {
                dataAdapter = new MySqlDataAdapter("select players.nickname, scores.score from((players inner join scores on players.id = scores.players_id) inner join games on scores.games_id = games.id) where games.name = \"Pacman\" order by scores.score desc;", conn);
            }
            MySqlCommandBuilder cmd = new MySqlCommandBuilder(dataAdapter);
            dataAdapter.Fill(Set, "AllScoresForGame");
        }

        // Adds the top scores per game
        public void GetTopScorePerGame()
        {
            // Opens connection, initializes new DataSet
            OpenConnection();
            Set = new DataSet();

            // Adds the top scores per game
            dataAdapter = new MySqlDataAdapter("select max(scores.score) from (scores inner join games on scores.games_id = games.id) where games.name = \"Super Mario\";", conn);
            MySqlCommandBuilder cmd = new MySqlCommandBuilder(dataAdapter);
            dataAdapter.Fill(Set, "AllTopScores");
            dataAdapter = new MySqlDataAdapter("select max(scores.score) from (scores inner join games on scores.games_id = games.id) where games.name = \"GTA\";", conn);
            MySqlCommandBuilder cmd2 = new MySqlCommandBuilder(dataAdapter);
            dataAdapter.Fill(Set, "AllTopScores");
            dataAdapter = new MySqlDataAdapter("select max(scores.score) from (scores inner join games on scores.games_id = games.id) where games.name = \"Pacman\";", conn);
            MySqlCommandBuilder cmd3 = new MySqlCommandBuilder(dataAdapter);
            dataAdapter.Fill(Set, "AllTopScores");
        }

        // Inserts parameter value for selected player
        public void GetPlayerScores(int selectedPlayer)
        {
            // Initialize new DataSet
            Set = new DataSet();

            // Get name of selected player
            if (selectedPlayer == 0) FillSetTopscorePerPlayer("\"Lorenzo\"");
            else if (selectedPlayer == 1) FillSetTopscorePerPlayer("\"Lorena\"");
            else if (selectedPlayer == 2) FillSetTopscorePerPlayer("\"Michael\"");
            else if (selectedPlayer == 3) FillSetTopscorePerPlayer("\"Sara\"");
        }

        // Fills DataSet with topscore values per game for the selected player
        private void FillSetTopscorePerPlayer(string name)
        {
            for (int i = 1; i <= 3; i++)
            {
                dataAdapter = new MySqlDataAdapter("select games.name, max(scores.score) from ((players inner join scores on players.id = scores.players_id) inner join games on scores.games_id = games.id) where players.name =" + name + "and games.id =" + i + ";", conn);
                MySqlCommandBuilder cmd = new MySqlCommandBuilder(dataAdapter);
                dataAdapter.Fill(Set, "PlayerTopScores");
            }

        }
    }
}
