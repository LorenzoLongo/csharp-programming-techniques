﻿using Globals.Interfaces;
using System.Collections.Generic;
using System.Data;
using System.Linq;

namespace LogicLayer
{
    // Class which contains the logic of the application
    public class Logic : ILogic
    {
        // Auto-implemented List properties (stringvalues)
        public List<string> PlayerNamesAVG { get; private set; }
        public List<string> GameNames { get; private set; }
        public List<string> PlayerTSList { get; private set; }
        public List<string> AllScores { get; private set; }
        public List<string> TopScorePerGame { get; private set; }

        // Auto-implemented integer properties (saves combobox index values)
        public int SelectedGameIndex { get; set; }
        public int SelectedPlayerIndex { get; set; }

        // field which temporarily contains string values
        private List<string> playerNames, avgScores, names, scores;

        // Three layer connection fields
        private readonly IConnection conn;
        private readonly IGame game;
        private readonly IPlayer player;

        // Constructor of Logic class
        public Logic(IConnection conn, IGame game, IPlayer player)
        {
            this.conn = conn;
            this.game = game;
            this.player = player;
            game.GetTopscoreAllGames(conn);
        }

        // Opens the connection and gets and sets the player and game names
        public void GetPlayersAndGames()
        {
            // Open connection, set player and game names
            conn.OpenConnection();
            conn.SetPlayersAndGamesName();

            // Inserts values of player name column and average score per player into a List
            playerNames = conn.Set.Tables[0].AsEnumerable().Select(r => r.Field<string>("name")).ToList();
            avgScores = conn.Set.Tables[0].AsEnumerable().Select(r => r.Field<decimal>("avg(scores.score)")).ToList().ConvertAll<string>(x => x.ToString());

            // Concat method to join the playerNames and avgScores Lists
            Concat(1);

            // Inserts values of the game name column
            GameNames = conn.Set.Tables[1].AsEnumerable().Select(r => r.Field<string>("name")).ToList();
            TopScorePerGame = game.AllTimeTopScore;
        }

        // Method which contains all scores for the selected game
        public void AllScoresForGame()
        {
            // Get selected game from game combobox in Form
            switch (SelectedGameIndex)
            {
                case 0:
                    conn.GetAllGameScores(0);
                    break;
                case 1:
                    conn.GetAllGameScores(1);
                    break;
                case 2:
                    conn.GetAllGameScores(2);
                    break;
            }
            // Inserts values of score column and player nickname into a List
            scores = conn.Set.Tables[0].AsEnumerable().Select(r => r.Field<int>("score")).ToList().ConvertAll<string>(x => x.ToString());
            names = conn.Set.Tables[0].AsEnumerable().Select(r => r.Field<string>("nickname")).ToList();
            Concat(2);
        }

        // Gets the topscores per game of the selected player
        public void PlayerTopScores()
        {
            switch (SelectedPlayerIndex)
            {
                case 0:
                    conn.GetPlayerScores(0);
                    break;
                case 1:
                    conn.GetPlayerScores(1);
                    break;
                case 2:
                    conn.GetPlayerScores(2);
                    break;
                case 3:
                    conn.GetPlayerScores(3);
                    break;
            }
            // Inserts values of topscore column and gamename into a List
            scores = conn.Set.Tables[0].AsEnumerable().Select(r => r.Field<int>("max(scores.score)")).ToList().ConvertAll<string>(x => x.ToString());
            names = conn.Set.Tables[0].AsEnumerable().Select(r => r.Field<string>("name")).ToList();
            Concat(3);
        }

        // Concats different Lists with values into a new List of strings
        private void Concat(int index)
        {
            int value = 0;
            if (index == 1)
            {
                PlayerNamesAVG = new List<string>();
                foreach (var i in playerNames)
                {
                    PlayerNamesAVG.Add(string.Format("{0, -20}{1, -15}", i, avgScores[value]));
                    value++;
                }
            }
            else if (index == 2)
            {
                AllScores = new List<string>();
                foreach (var i in names)
                {
                    AllScores.Add(string.Format("{0, -20}{1, -5}", scores[value], i));
                    value++;
                }
            }
            else if (index == 3)
            {
                PlayerTSList = new List<string>();
                foreach (var i in names)
                {
                    PlayerTSList.Add(string.Format("{0, -15}{1, -15}{2, -5}", TopScorePerGame[value], scores[value], i));
                    value++;
                }
            }
        }
    }
}
