﻿namespace HighScoresMain
{
    partial class Form1
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.allPlayersComboBox = new System.Windows.Forms.ComboBox();
            this.lblPlayer = new System.Windows.Forms.Label();
            this.scoresPerGameListBox = new System.Windows.Forms.ListBox();
            this.scoresPerPlayerListBox = new System.Windows.Forms.ListBox();
            this.allGamesComboBox = new System.Windows.Forms.ComboBox();
            this.lblGame = new System.Windows.Forms.Label();
            this.lblOverallTS = new System.Windows.Forms.Label();
            this.lblPlayerTS = new System.Windows.Forms.Label();
            this.lblGamePP = new System.Windows.Forms.Label();
            this.lblScore = new System.Windows.Forms.Label();
            this.lbGameG = new System.Windows.Forms.Label();
            this.SuspendLayout();
            // 
            // allPlayersComboBox
            // 
            this.allPlayersComboBox.FormattingEnabled = true;
            this.allPlayersComboBox.Location = new System.Drawing.Point(25, 68);
            this.allPlayersComboBox.Name = "allPlayersComboBox";
            this.allPlayersComboBox.Size = new System.Drawing.Size(183, 21);
            this.allPlayersComboBox.TabIndex = 0;
            this.allPlayersComboBox.SelectedIndexChanged += new System.EventHandler(this.allPlayersComboBox_SelectedIndexChanged);
            // 
            // lblPlayer
            // 
            this.lblPlayer.AutoSize = true;
            this.lblPlayer.Location = new System.Drawing.Point(22, 52);
            this.lblPlayer.Name = "lblPlayer";
            this.lblPlayer.Size = new System.Drawing.Size(39, 13);
            this.lblPlayer.TabIndex = 1;
            this.lblPlayer.Text = "Player:";
            // 
            // scoresPerGameListBox
            // 
            this.scoresPerGameListBox.FormattingEnabled = true;
            this.scoresPerGameListBox.Location = new System.Drawing.Point(25, 117);
            this.scoresPerGameListBox.Name = "scoresPerGameListBox";
            this.scoresPerGameListBox.Size = new System.Drawing.Size(183, 238);
            this.scoresPerGameListBox.TabIndex = 2;
            // 
            // scoresPerPlayerListBox
            // 
            this.scoresPerPlayerListBox.FormattingEnabled = true;
            this.scoresPerPlayerListBox.Location = new System.Drawing.Point(290, 117);
            this.scoresPerPlayerListBox.Name = "scoresPerPlayerListBox";
            this.scoresPerPlayerListBox.Size = new System.Drawing.Size(183, 238);
            this.scoresPerPlayerListBox.TabIndex = 3;
            // 
            // allGamesComboBox
            // 
            this.allGamesComboBox.FormattingEnabled = true;
            this.allGamesComboBox.Location = new System.Drawing.Point(290, 68);
            this.allGamesComboBox.Name = "allGamesComboBox";
            this.allGamesComboBox.Size = new System.Drawing.Size(183, 21);
            this.allGamesComboBox.TabIndex = 4;
            this.allGamesComboBox.SelectedIndexChanged += new System.EventHandler(this.allGamesComboBox_SelectedIndexChanged);
            // 
            // lblGame
            // 
            this.lblGame.AutoSize = true;
            this.lblGame.Location = new System.Drawing.Point(287, 52);
            this.lblGame.Name = "lblGame";
            this.lblGame.Size = new System.Drawing.Size(38, 13);
            this.lblGame.TabIndex = 5;
            this.lblGame.Text = "Game:";
            // 
            // lblOverallTS
            // 
            this.lblOverallTS.AutoSize = true;
            this.lblOverallTS.Location = new System.Drawing.Point(26, 101);
            this.lblOverallTS.Name = "lblOverallTS";
            this.lblOverallTS.Size = new System.Drawing.Size(40, 13);
            this.lblOverallTS.TabIndex = 6;
            this.lblOverallTS.Text = "Overall";
            // 
            // lblPlayerTS
            // 
            this.lblPlayerTS.AutoSize = true;
            this.lblPlayerTS.Location = new System.Drawing.Point(79, 101);
            this.lblPlayerTS.Name = "lblPlayerTS";
            this.lblPlayerTS.Size = new System.Drawing.Size(52, 13);
            this.lblPlayerTS.TabIndex = 7;
            this.lblPlayerTS.Text = "Topscore";
            // 
            // lblGamePP
            // 
            this.lblGamePP.AutoSize = true;
            this.lblGamePP.Location = new System.Drawing.Point(134, 101);
            this.lblGamePP.Name = "lblGamePP";
            this.lblGamePP.Size = new System.Drawing.Size(35, 13);
            this.lblGamePP.TabIndex = 8;
            this.lblGamePP.Text = "Game";
            // 
            // lblScore
            // 
            this.lblScore.AutoSize = true;
            this.lblScore.Location = new System.Drawing.Point(290, 101);
            this.lblScore.Name = "lblScore";
            this.lblScore.Size = new System.Drawing.Size(35, 13);
            this.lblScore.TabIndex = 9;
            this.lblScore.Text = "Score";
            // 
            // lbGameG
            // 
            this.lbGameG.AutoSize = true;
            this.lbGameG.Location = new System.Drawing.Point(358, 101);
            this.lbGameG.Name = "lbGameG";
            this.lbGameG.Size = new System.Drawing.Size(35, 13);
            this.lbGameG.TabIndex = 10;
            this.lbGameG.Text = "Game";
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(499, 367);
            this.Controls.Add(this.lbGameG);
            this.Controls.Add(this.lblScore);
            this.Controls.Add(this.lblGamePP);
            this.Controls.Add(this.lblPlayerTS);
            this.Controls.Add(this.lblOverallTS);
            this.Controls.Add(this.lblGame);
            this.Controls.Add(this.allGamesComboBox);
            this.Controls.Add(this.scoresPerPlayerListBox);
            this.Controls.Add(this.scoresPerGameListBox);
            this.Controls.Add(this.lblPlayer);
            this.Controls.Add(this.allPlayersComboBox);
            this.Name = "Form1";
            this.Text = "HighScores ";
            this.Load += new System.EventHandler(this.Form1_Load);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.ComboBox allPlayersComboBox;
        private System.Windows.Forms.Label lblPlayer;
        private System.Windows.Forms.ListBox scoresPerGameListBox;
        private System.Windows.Forms.ListBox scoresPerPlayerListBox;
        private System.Windows.Forms.ComboBox allGamesComboBox;
        private System.Windows.Forms.Label lblGame;
        private System.Windows.Forms.Label lblOverallTS;
        private System.Windows.Forms.Label lblPlayerTS;
        private System.Windows.Forms.Label lblGamePP;
        private System.Windows.Forms.Label lblScore;
        private System.Windows.Forms.Label lbGameG;
    }
}

