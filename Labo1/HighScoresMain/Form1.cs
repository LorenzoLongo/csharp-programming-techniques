﻿using Globals.Interfaces;
using System;
using System.Windows.Forms;

namespace HighScoresMain
{
    public partial class Form1 : Form
    {
        // Private fields
        private readonly ILogic logic;
        private BindingSource bs;

        // Constructor of Form class
        public Form1(ILogic logic)
        {
            this.logic = logic;
            InitializeComponent();
        }

        // Loads all necessairy data into the form
        private void Form1_Load(object sender, EventArgs e)
        {
            // Inserts all of the player names sorted on average
            logic.GetPlayersAndGames();
            bs = new BindingSource();
            bs.DataSource = logic.PlayerNamesAVG;
            allPlayersComboBox.DataSource = bs;

            // Inserts all of the game names
            bs = new BindingSource();
            bs.DataSource = logic.GameNames;
            allGamesComboBox.DataSource = bs;

            // Inserts the left Listbox which contains the overall topscores, selected player topscores per game
            logic.PlayerTopScores();
            bs = new BindingSource();
            bs.DataSource = logic.PlayerTSList;
            scoresPerGameListBox.DataSource = bs;
        }

        // Updates values of Listbox if selected game is changed
        private void allGamesComboBox_SelectedIndexChanged(object sender, EventArgs e)
        {
            logic.SelectedGameIndex = allGamesComboBox.SelectedIndex;
            logic.AllScoresForGame();
            bs = new BindingSource();
            bs.DataSource = logic.AllScores;
            scoresPerPlayerListBox.DataSource = bs;
        }

        // Updates values of Listbox if selected player is changed
        private void allPlayersComboBox_SelectedIndexChanged(object sender, EventArgs e)
        {
            logic.SelectedPlayerIndex = allPlayersComboBox.SelectedIndex;
            logic.PlayerTopScores();
            bs = new BindingSource();
            bs.DataSource = logic.PlayerTSList;
            scoresPerGameListBox.DataSource = bs;
        }
    }
}
